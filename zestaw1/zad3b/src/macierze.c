#include "functions.h"

int main(int argc, char* argv[])
{
	if (argc<3)
	{
		printf("Usage: %s file mul|add\n", argv[0]);
		return 1;
	}

	int operation = 0;
	if(strcmp(argv[2], "mul") == 0) operation = 1;
	else if(strcmp(argv[2], "add") == 0) operation = 2;
	else{
		printf("Usage: %s file mul|add\n", argv[0]);
		return 3;
	}

	//otwarcie pliku
	FILE* f = fopen(argv[1], "r");

	if(f==0)
	{
		printf("Could not open file %s\n", argv[1]);
		return 2;
	}

	MATRIX matrix1 = read_matrix(f);
	MATRIX matrix2 = read_matrix(f);
	printf("Matrix 1\n");
	print_matrix(&matrix1);
	printf("\nMatrix 2\n");
	print_matrix(&matrix2);
	
	MATRIX matrix3;

	int error;
	if(operation == 1){
		error = multiply_matrices(&matrix1, &matrix2, &matrix3);
		if(!error) printf("\nmultiplied:\n");
	}
	else if(operation == 2){
		error = add_matrices(&matrix1, &matrix2, &matrix3);
		if(!error) printf("\nadded:\n");
	}

	if(!error) print_matrix(&matrix3);
			
	return 0;
}

