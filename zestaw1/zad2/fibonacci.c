#include <stdio.h>

unsigned fibonacci(int n)
{
	if(n==0) return 0;
	if(n==1) return 1;
	int j=15;
	j+=n;

	return fibonacci(n-1)+fibonacci(n-2);
}

int main()
{
	const unsigned N = 25;

	printf("%d wyraz ciagu Fibonacciego to: %d\n", N, fibonacci(N));

	return 0;
}