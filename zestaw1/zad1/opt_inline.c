#include <stdio.h>
#include <stdlib.h>

int hash(int j)
{
	return j = j*100%12345;
}

int hash2(int j)
{
    return j = j<<2;
}

int hash3(int j)
{
    return j>>4;
}

int main(int argc, char ** argv)
{
	long long i, j, count;

	j = 1;
	count = (1000*1000*1000LL);
	for(i = 0; i < count; i++)
    {
		j = hash(j);
        j = hash2(j);
        j = hash3(j);
    }
	printf("j = %lld\n", j);
    return 0;
}
