#!/bin/bash

echo 'OPTIMIZATION LEVEL 1 TEST'
echo 'No optimization:'
time -p ./opt1_0
echo 'O1 optimization:'	
time -p ./opt1_1
echo 'No optimization, architecture specified:'
time -p ./opt1_0_cpu
echo 'O1 optimization, architecture specified:'
time -p ./opt1_1_cpu
echo

echo 'SPACE OPTIMIZATION TEST'
echo 'No optimization:'
time -p ./opts_0
echo 'Os optimization:'	
time -p ./opts_s
echo 'No optimization, architecture specified:'
time -p ./opts_0_cpu
echo 'Os optimization, architecture specified:'
time -p ./opts_s_cpu
echo

echo '-finline-small-functions TEST'
echo 'Option off:'
time -p ./opt_inline_f
echo 'Option on:'	
time -p ./opt_inline_t
echo 'Option off, architecture specified:'
time -p ./opt_inline_f_cpu
echo 'Option on, architecture specified:'
time -p ./opt_inline_t_cpu
echo

echo '-funroll-loops TEST'
echo 'Option off:'
time -p ./opt_loops_no_unroll
echo 'Option on:'	
time -p ./opt_loops_unroll
echo 'Option off, architecture specified:'
time -p ./opt_loops_no_unroll_cpu
echo 'Option on, architecture specified:'
time -p ./opt_loops_unroll_cpu