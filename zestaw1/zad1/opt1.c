 
    #include <stdio.h>
    
    double power(double d, unsigned n);

    int main()
    {
        double sum = 0.0;
        
        int i;
        for(i=0; i< 100000000; i++)
            sum+=power(i, i%5);

        printf("sum=%.3f\n", sum);
        return 0;   
    }

    //iteracyjne potegowanie liczby zmiennoprzecinkowej
    double power(double d, unsigned n)
    {
            double result = 1.0;

            int j;
            for(j = 0; j< n; j++)
                    result *= d;

            return result;
    }

