#include "zad2.h"

int shm_desc;
sem_t *mutex, *db_lock, *readers_lock;
int *writer_in, *readers_in, *readers_pending;
int urandom_desc;
int i;

int main(int argc, char const *argv[])
{
	if(argc < 2){
		printf("Usage: %s writers readers\n", argv[0]);
		exit(1);
	}

	int readers, writers;
	sscanf(argv[1], "%d", &writers);
	sscanf(argv[2], "%d", &readers);

	/* open /dev/urandom */
	urandom_desc = open("/dev/urandom", O_RDONLY);

	/* create and initialize shared memory object */
	shm_desc = shm_open("shared_mem", O_CREAT | O_RDWR, 0600);
	if(shm_desc == -1){
		perror("shm_open");
		exit(errno);
	}

	if(ftruncate(shm_desc, sizeof(int)*ARRAY_SIZE + 3*sizeof(int)) == -1){
		perror("ftruncate");
		exit(errno);
	}

	int* shm_area = (int*)mmap(NULL, sizeof(int)*ARRAY_SIZE + 3*sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shm_desc, 0);
	if(shm_area == MAP_FAILED){
		perror("mmap");
		exit(errno);
	}

	writer_in = shm_area;
	readers_in = shm_area + 1;
	readers_pending = shm_area + 2;
	shm_area = shm_area + 3;

	*writer_in = 0;
	*readers_in = 0;
	*readers_pending = 0;

	/* initialize array with random data */
	for(i=0; i<ARRAY_SIZE; i++){
		read(urandom_desc, (char*)(shm_area + i), sizeof(int));
		shm_area[i] = shm_area[i] >= 0 ? shm_area[i] : -shm_area[i];
		shm_area[i] = shm_area[i] % (UP_BOUND - DOWN_BOUND) + DOWN_BOUND;
	}

	munmap(shm_area, sizeof(int)*ARRAY_SIZE + 3*sizeof(int));

	/* initialize semaphores */
	mutex = sem_open("sem_mutex", O_CREAT, 0600, 1);
	db_lock = sem_open("sem_wr_pending", O_CREAT, 0600, 1);
	readers_lock = sem_open("sem_read_blk", O_CREAT, 0600, 0);

	pid_t pid;

	/* readers */
	for(i=0; i<readers; i++){
		pid = fork();
		if(pid == 0){
			/* in child */

			/* generate number to be searched */
			int searched_num;
			read(urandom_desc, (char*)&searched_num, sizeof(int));
			searched_num = searched_num >= 0 ? searched_num : -searched_num;
			searched_num = searched_num % (UP_BOUND - DOWN_BOUND) + DOWN_BOUND;

			/* map shared memory */
			shm_area = (int*)mmap(NULL, sizeof(int)*ARRAY_SIZE + 3*sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shm_desc, 0);
			if(shm_area == MAP_FAILED){
				perror("mmap");
				exit(errno);
			}

			writer_in = shm_area;
			readers_in = shm_area + 1;
			readers_pending = shm_area + 2;
			shm_area = shm_area + 3;

			mutex = sem_open("sem_mutex", 0);
			db_lock = sem_open("sem_wr_pending", 0);
			readers_lock = sem_open("sem_read_blk", 0);

			/* set parent death signal */
			prctl(PR_SET_PDEATHSIG, SIGHUP);

			while(1){

				/* new pending reader */
				sem_wait(mutex);
				*readers_pending = *readers_pending + 1;

				/* if there is no writer */
				if(*writer_in == 0){
					/* next reader in database */
					*readers_in = *readers_in + 1;

					/* if first reader, lock db */
					if(*readers_in == 1) sem_wait(db_lock);
					sem_post(mutex);
				}
				else{
					sem_post(mutex);
					/* wait until writer finishes */
					sem_wait(readers_lock);

					/* next reader in database */
					*readers_in = *readers_in + 1;

					/* let wainting readers in db */
					if(*readers_pending > *readers_in) sem_post(readers_lock);
					else sem_post(mutex);
				}

				/* search for number */
				int j, found_nums = 0;
				for(j=0; j<ARRAY_SIZE; j++){
					if(shm_area[j] == searched_num) found_nums++;
				}
				printf("Reader: number %d found in database %d times\n", searched_num, found_nums);

				/* reader leaves database */
				sem_wait(mutex);
				*readers_in = *readers_in - 1;
				*readers_pending = *readers_pending - 1;

				if(*readers_in == 0) sem_post(db_lock);

				sem_post(mutex);

				sleep(1);
			}

			exit(0);
		}
		else if(pid < 0){
			perror("fork");
			exit(errno);
		}
	}

	for(i=0; i<writers; i++){
		pid = fork();
		if(pid == 0){
			/* in child */

			/* map shared memory */
			shm_area = (int*)mmap(NULL, sizeof(int)*ARRAY_SIZE + 3*sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shm_desc, 0);
			if(shm_area == MAP_FAILED){
				perror("mmap");
				exit(errno);
			}

			writer_in = shm_area;
			readers_in = shm_area + 1;
			readers_pending = shm_area + 2;
			shm_area = shm_area + 3;

			mutex = sem_open("sem_mutex", 0);
			db_lock = sem_open("sem_wr_pending", 0);
			readers_lock = sem_open("sem_read_blk", 0);

			/* set parent death signal */
			prctl(PR_SET_PDEATHSIG, SIGHUP);

			while(1){

				/* how many indices to modify */
				int nums_to_modyfy;
				read(urandom_desc, (char*)&nums_to_modyfy, sizeof(int));
				nums_to_modyfy = nums_to_modyfy >= 0 ? nums_to_modyfy : -nums_to_modyfy;
				nums_to_modyfy = nums_to_modyfy % ARRAY_SIZE;

				/* generate indices to modify*/
				int* to_modify = (int*)malloc(sizeof(int)*nums_to_modyfy);
				int j;
				for(j=0; j<nums_to_modyfy; j++){
					int in_arr = 0;
					do{
						in_arr = 0;

						/* generate index */
						read(urandom_desc, (char*)(to_modify+j), sizeof(int));
						to_modify[j] = to_modify[j] >= 0 ? to_modify[j] : -to_modify[j];
						to_modify[j] = to_modify[j] % ARRAY_SIZE;
						
						/* check if it is not in array already */
						int k;
						for(k=0; k<j; k++){
							if(to_modify[k] == to_modify[j]){
								in_arr = 1;
								break;
							}
						}
					} while(in_arr);
				}

				/* writer enters database */
				sem_wait(mutex);
				*writer_in = *writer_in + 1;
				sem_post(mutex);

				/* access to resource */
				sem_wait(db_lock);

				/* modify selected elements */
				for(j=0; j<nums_to_modyfy; j++){
					int new_value;
					read(urandom_desc, (char*)&new_value, sizeof(int));
					new_value = new_value >= 0 ? new_value : -new_value;
					new_value = new_value % (UP_BOUND - DOWN_BOUND) + DOWN_BOUND;

					shm_area[to_modify[j]] = new_value;
				}

				sem_post(db_lock);

				sem_wait(mutex);
				*writer_in = *writer_in - 1;	

				/* let readers in */
				if(*writer_in == 0 && *readers_in > 0){
					sem_wait(db_lock);
					sem_post(readers_lock);
				}
				else sem_post(mutex);

				free(to_modify);

				printf("Producer: array modified\n");

				sleep(1);
			}

			exit(0);
		}
	}

	signal(SIGINT, cleanup);

	while(1);

	return 0;
}

void cleanup(int param)
{
	sem_unlink("sem_mutex");
	sem_close(mutex);

	sem_unlink("sem_wr_pending");
	sem_close(db_lock);

	sem_unlink("sem_read_blk");
	sem_close(readers_lock);
	exit(0);
}