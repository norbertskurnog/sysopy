#ifndef ZAD2_H
#define ZAD2_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <signal.h>

#include <sys/prctl.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <semaphore.h>

#include <errno.h>

#define ARRAY_SIZE 100
#define DOWN_BOUND -100
#define UP_BOUND 100

void cleanup(int param);

#endif