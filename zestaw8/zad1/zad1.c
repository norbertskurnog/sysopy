#include "zad1.h"

double* tasks;
int *tasks_count, *tasks_head, *tasks_tail;

int shm_id, mutex_id, semfull_id, semempty_id;

int urandom_desc;

int i;

int main(int argc, char const *argv[])
{
	if(argc < 3){
		printf("Usage: %s producers consumers\n", argv[0]);
		exit(1);
	}	

	int producers, consumers;
	sscanf(argv[1], "%d", &producers);
	sscanf(argv[2], "%d", &consumers);
	
	/* shared memory initialization */
	key_t shm_key = ftok("/tmp", SHM_ID);
	if(shm_key == (key_t)-1){
		perror("ftok");
		exit(errno);
	}
	
	size_t shm_size = sizeof(double)*ARRAY_MAX_SIZE*TASK_SIZE + 3*sizeof(int);
	shm_id = shmget(shm_key, shm_size, IPC_CREAT | 0600);
	if(shm_id == -1){
		perror("shmget create");
		exit(errno);
	}

	void* shm_area = shmat(shm_id, NULL, 0);
	if(shm_area == (void*)-1){
		perror("shmat");
		exit(errno);
	}

	tasks_count = (int*)shm_area;
	tasks_head = (int*)(shm_area + sizeof(int));
	tasks_tail = (int*)(shm_area + 2*sizeof(int));
	tasks = shm_area + 3*sizeof(int);

	*tasks_count = 0;	
	*tasks_head = -1; 
	*tasks_tail = -1;
	
	shmdt(shm_area);

	/* semaphore initialization */
	key_t semm_key = ftok("/tmp", SEMM_ID);
	key_t seme_key = ftok("/tmp", SEME_ID);
	key_t semf_key = ftok("/tmp", SEMF_ID);
	if(semm_key == (key_t)-1 || seme_key == (key_t)-1 || semf_key == (key_t)-1){
		perror("ftok");
		exit(errno);
	}

	mutex_id = semget(semm_key, 1, IPC_CREAT | 0600);
	semempty_id = semget(seme_key, 1, IPC_CREAT | 0600);
	semfull_id = semget(semf_key, 1, IPC_CREAT | 0600);
	if(mutex_id == -1 || semempty_id == -1 || semfull_id == -1){
		perror("semget create");
		exit(errno);
	}

	semctl(mutex_id, 0, SETVAL, 1);
	semctl(semfull_id, 0, SETVAL, 0);
	semctl(semempty_id, 0, SETVAL, 0);

	pid_t pid;

	/* RNG init */
	urandom_desc = open("/dev/urandom", O_RDONLY);

	/* create producer processes */
	for(i=0; i<producers; i++){
		pid = fork();
		if(pid == 0){
			/* in child */

			shm_id = shmget(shm_key, shm_size, 0600);
			if(shm_id == -1){
				perror("shmget");
				exit(errno);
			}

			mutex_id = semget(semm_key, 0, 0600);
			semempty_id = semget(seme_key, 1, 0600);
			semfull_id = semget(semf_key, 1, 0600);
			if(mutex_id == -1 || semfull_id == -1 || semempty_id == -1){
				perror("semget");
				exit(errno);
			}

			/* access shared memory */
			shm_area = (void*)shmat(shm_id, NULL, 0);
			if(tasks == (void*)-1){
				perror("shmat");
				exit(errno);
			}

			tasks_count = (int*)shm_area;
			tasks_head = (int*)(shm_area + sizeof(int));
			tasks_tail = (int*)(shm_area + 2*sizeof(int));
			tasks = shm_area + 3*sizeof(int);

			/* exit when parent exits */
			prctl(PR_SET_PDEATHSIG, SIGHUP);

			/* control struct */
			struct sembuf sb;
			sb.sem_num = 0;
			sb.sem_flg = 0;

			while(1){
				/* wait until there is free space in buffer */
				sb.sem_op = 0;
				semop(semfull_id, &sb, 1);

				/* lock mutex */
				sb.sem_op = -1;
				semop(mutex_id, &sb, 1);

				/* calculate index */
				if(*tasks_count < ARRAY_MAX_SIZE){
					if(*tasks_head == -1 || *tasks_tail == -1){
						*tasks_tail = 0;
						*tasks_head = 0;
					}
					else if(*tasks_head == ARRAY_MAX_SIZE-1){
						*tasks_head = 0;
					}
					else{
						*tasks_head += 1;
					}

					*tasks_count += 1;
				}
				else{
					/* release mutex */
					sb.sem_op = 1;
					semop(mutex_id, &sb, 1);

					/* array is full, wait */
					if(!semctl(semfull_id, 0, GETVAL)){
						sb.sem_op = 1;
						semop(semfull_id, &sb, 1);
					}

					continue;
				}

				/* create and put new task */
				int rnd_num = 0;
				double rnd_double;
				for(i=0; i<TASK_SIZE; i++){				
					read(urandom_desc, (char*)&rnd_num, sizeof(int));
					rnd_double = (double)(rnd_num % 10000000)/10000;
					tasks[*tasks_head + ARRAY_MAX_SIZE*i] = rnd_double;
				}

				printf("Producer: task put in arrray.\n");

				/* release mutex */
				sb.sem_op = 1;
				semop(mutex_id, &sb, 1);

				/* buffer is not empty anymore, relase empty sem if it was set before*/
				if(semctl(semempty_id, 0, GETVAL)){
					sb.sem_op = -1;
					semop(semempty_id, &sb, 1);
				}
			}
		}
		else if(pid < 0){
			perror("fork");
			exit(errno);
		}
	}

	for(i=0; i<consumers; i++){
		pid = fork();
		if(pid == 0){
			/* in child */

			shm_id = shmget(shm_key, shm_size, 0600);
			if(shm_id == -1){
				perror("shmget");
				exit(errno);
			}

			mutex_id = semget(semm_key, 1, 0600);
			semempty_id = semget(seme_key, 1, 0600);
			semfull_id = semget(semf_key, 1, 0600);
			if(mutex_id == -1 || semfull_id == -1 || semempty_id == -1){
				perror("semget");
				exit(errno);
			}

			/* access shared memory */
			shm_area = (void*)shmat(shm_id, NULL, 0);
			if(tasks == (void*)-1){
				perror("shmat");
				exit(errno);
			}

			tasks_count = (int*)shm_area;
			tasks_head = (int*)(shm_area + sizeof(int));
			tasks_tail = (int*)(shm_area + 2*sizeof(int));
			tasks = shm_area + 3*sizeof(int);

			/* exit when parent exits */
			prctl(PR_SET_PDEATHSIG, SIGHUP);

			/* sem control struct */
			struct sembuf sb;
			sb.sem_num = 0;
			sb.sem_flg = 0;

			while(1){
				/* wait until buffer is not empty */
				sb.sem_op = 0;
				semop(semempty_id, &sb, 1);

				/* lock mutex */
				sb.sem_op = -1;
				semop(mutex_id, &sb, 1);

				/* calculate index in task array */
				int task_ind;
				if(*tasks_count > 0){
					task_ind = *tasks_tail;

					if(*tasks_tail == ARRAY_MAX_SIZE-1){
						*tasks_tail = 0;
					}
					else{
						*tasks_tail += 1;
					}
				}
				else{
					/* if empty, release mutex... */
					sb.sem_op = 1;
					semop(mutex_id, &sb, 1);

					/* and wait until not empty */
					if(!semctl(semempty_id, 0, GETVAL)){
						sb.sem_op = 1;
						semop(semempty_id, &sb, 1);
					}
					continue;
				} 

				*tasks_count -= 1;
				//printf("Consumer: read %f DEBUG head=%d, tail=%d, count=%d\n", tasks[old_tail], *tasks_head, *tasks_tail, *tasks_count);
				
				/* solve task, print result */
				double sum = 0, min = tasks[task_ind], max = min;

				for(i=0; i<TASK_SIZE; i++){
					if(tasks[task_ind + ARRAY_MAX_SIZE*i] < min)
						min = tasks[task_ind + ARRAY_MAX_SIZE*i];
					if(tasks[task_ind + ARRAY_MAX_SIZE*i] > max)
						max = tasks[task_ind + ARRAY_MAX_SIZE*i];
					sum += tasks[task_ind + ARRAY_MAX_SIZE*i];
				}

				printf("Task solved: min=%f, max=%f, sum=%f\n", min, max, sum);

				/* release mutex */
				sb.sem_op = 1;
				semop(mutex_id, &sb, 1);
				
				/* release full semaphore, buffer is not full anymore */
				if(semctl(semfull_id, 0, GETVAL)){
					sb.sem_op = -1;
					semop(semfull_id, &sb, 1);
				}
			}
			break;
		}
		else if(pid < 0){
			perror("fork");
			exit(errno);
		}
	}

	signal(SIGINT, cleanup);

	while(1);

	return 0;
}

void cleanup(int param)
{
	shmctl(shm_id, IPC_RMID, 0);
	semctl(mutex_id, IPC_RMID, 0);
	semctl(semfull_id, IPC_RMID, 0);
	semctl(semempty_id, IPC_RMID, 0);
	
	close(urandom_desc);

	exit(0);
}