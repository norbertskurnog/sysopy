#ifndef COMMON_H
#define COMMON_H

#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/prctl.h>

#include <stdlib.h>
#include <stdio.h>

#include <signal.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define SHM_ID 61
#define SEMM_ID 70
#define SEME_ID 80
#define SEMF_ID 90

#define ARRAY_MAX_SIZE 25
#define TASK_SIZE 25

void cleanup(int param);

#endif