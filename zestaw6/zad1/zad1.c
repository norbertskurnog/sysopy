#include "zad1.h"

int main(int argc, char** argv)
{
	/* parsing cmd line */
	if(argc < 3){
		printf("Usage: %s input output\n", argv[0]);
		return 1;
	}

	/* opening files */
	int input_desc = open(argv[1], O_RDONLY);
	if(input_desc == -1){
		printf("Cannot open file %s\n", argv[1]);
		return errno;
	}

	int output_desc = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC, S_IRWXU);
	if(output_desc == -1){
		printf("Cannot open file %s\n", argv[2]);
		return errno;
	}

	int pipe_desc[2];
	pipe(pipe_desc);

	pid_t pid = fork();

	if(pid == 0){
		/* in child */
		close(pipe_desc[0]);

		dup2(input_desc, STDIN_FILENO); /* input > stdin */
		close(input_desc);

		dup2(pipe_desc[1], STDOUT_FILENO); /* stdout -> pipe */
		close(pipe_desc[1]);
		
		char* arg[] = {"gzip", "-9", "-fc", "-", NULL};
		execvp(arg[0], arg);

		printf("Cannot exec gzip!\n");
		return errno;
	}
	else if (pid > 0){
		/* in parent */
		close(pipe_desc[1]);

		int status;
		wait(&status);

		//printf("Finished: %d\n", WEXITSTATUS(status));

		dup2(pipe_desc[0], STDIN_FILENO); /* pipe -> stdin */
		close(pipe_desc[0]);

		dup2(output_desc, STDOUT_FILENO); /* stdout -> output */
		close(output_desc);

		char* arg[] = {"base64", NULL};
		execvp(arg[0], arg);

		printf("Cannot exec base64!\n");
		return errno;

	}
	else{
		printf("Cannot fork!\n");
		return errno;
	}

	return 0;
}