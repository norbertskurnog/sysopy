#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

int main(int argc, char** argv)
{
	/* parse cmd line */
	if(argc < 2){
		printf("Usage: %s fifo_name\n", argv[0]);
		return 1;
	}

	mkfifo(argv[1], S_IRWXU);
	int fifo = open(argv[1], O_RDONLY|O_TRUNC);
	if(fifo == -1){
		printf("Cannot create fifo!\n");
		return 2;
	}

	char buffer[255], message[255], filename[255];

	char *tmp = NULL;
	int bytes_read = 0;
	while(1){
		bytes_read = read(fifo, buffer, sizeof(buffer));
		if(bytes_read > 0){
			/* read first line */
			tmp = strtok(buffer, "\n"); /* remove new line character */
			strcpy(message, tmp);
			memset(buffer, 0, sizeof(buffer));

			read(fifo, buffer, sizeof(buffer));
			tmp = strtok(buffer, "\n");
			strcpy(filename, tmp);

			int fd = open(filename, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU);
			if(fd == -1){
				printf("Cannot open file %s\n", filename);
				return errno;
			}

			char* datetime;
			clock_t curr_time = time(NULL);
			datetime = ctime(&curr_time);

			sprintf(buffer, "%s%s\n", datetime, message);
			write(fd, buffer, strlen(buffer));
			close(fd);
		}
	}

	unlink(argv[1]);

	return 0;
}
