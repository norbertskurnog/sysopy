#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
	/* parse cmd line */
	if(argc < 3){
		printf("Usage: %s fifo file\n", argv[0]);
		return 1;
	}

	int fifo = open(argv[1], O_WRONLY);
	if(fifo == -1){
		printf("Cannot open file %s\n", argv[1]);
		return 2;
	}

	while(1){
		char msg[255];
		memset(&msg, 0, sizeof(msg));
		read(STDIN_FILENO, &msg, sizeof(msg));

		write(fifo, &msg, sizeof(msg));
		write(fifo, argv[2], strlen(argv[2]));
	}

	return 0;
}