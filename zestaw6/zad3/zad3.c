#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char** argv)
{

	FILE* pipe = popen("gzip - > message.gz", "w");

	char c = 0;
	while(c != EOF){
		c = getc(stdin);
		fwrite(&c, sizeof(c), 1, pipe);
	}
	return 0;
}