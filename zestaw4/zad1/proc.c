#include "proc.h"

extern unsigned* licznik;

int main(int argc, char* argv[])
{
	unsigned N;

	struct tms time_beg, time_end;
	clock_t real_beg = times(&time_beg);

	if(argc<2){
		printf("Usage: %s N\n", argv[0]);
		exit(1);
	}

	N = atoi(argv[1]);
	if(N<1){
		printf("N must be > 0!\n");
		exit(2);
	}

	clock_t real_chld = 0;


	/* Create POSIX shared memory object */
	#ifdef FILE_MAPPING
	int shmdesc = shm_open("/counter_shm", O_RDWR|O_CREAT,  S_IRUSR|S_IWUSR);
	if(shmdesc == -1){
		printf("Failed to create shared memory object\n");
		exit(errno);
	}

	int error = ftruncate(shmdesc, sizeof(unsigned));
	if(error == -1){
		printf("Failed to set shared memory size\n");
		exit(errno);
	}

	licznik = mmap(NULL, sizeof(unsigned), PROT_WRITE|PROT_READ, MAP_SHARED, shmdesc, 0);
	#else
	/*MAP_ANON memory mapping is not backended by any file, supported in linux since 2.4 
	  all elements are automatically initialized with zeros*/
	licznik = mmap(NULL, sizeof(unsigned), PROT_WRITE|PROT_READ, MAP_ANON|MAP_SHARED, -1, 0);
	#endif

	if(licznik == MAP_FAILED){
	 	printf("Shared memory mapping failed\n");
	 	exit(errno);
	}

	#if defined(VCLONE) || defined(FCLONE)
	void* child_stack = malloc(16384);
	child_stack += 16384;  /*stack grows to lower adresses */
	int (*fn)(void*) = child;
	#endif

	int i;
	for(i=0; i<N; i++){
		int status;

		#ifdef FORK
		pid_t pid = fork();
		#endif

		#ifdef VFORK
		pid_t pid = vfork();
		#endif 

		#ifdef FCLONE
		pid_t pid = clone(fn, child_stack, SIGCHLD, NULL);
		#endif
		#ifdef VCLONE
		pid_t pid = clone(fn, child_stack, SIGCHLD | CLONE_VM | CLONE_VFORK | CLONE_FS | CLONE_FILES, NULL);
		#endif

		if(pid > 0){
			//parent, wait for child process to end
			//waitpid(pid, NULL, 0);
			wait(&status);
			real_chld += WEXITSTATUS(status);

		}
		else if(pid == 0){
			clock_t time_b = clock();
			child();
			clock_t time_e = clock();
			_exit(time_e - time_b);
		}
		else{
			printf("Failed to fork process %d\n", i);
		}
	}

	clock_t real_end = times(&time_end);

	double clktck=sysconf(_SC_CLK_TCK);
	// printf("N=%d\nReal: %.2f\nUsr: %.2f\nSys: %.2f\nChild usr: %.2f\nChild sys: %.2f\n\n",
	// 	N,
	// 	(real_end - real_beg)/clktck,
	// 	(time_end.tms_utime - time_beg.tms_utime)/clktck, 
	// 	(time_end.tms_stime - time_beg.tms_stime)/clktck,
	// 	(time_end.tms_cutime - time_beg.tms_cutime)/clktck, 
	// 	(time_end.tms_cstime - time_beg.tms_cstime)/clktck);
	printf("Child real %.2f\n", real_chld/clktck);

	//printf("Licznik: %d\n", *licznik);

	#ifdef FILE_MAPPING
	shmdesc = shm_unlink("/counter_shm");
	#endif

	munmap(licznik, sizeof(unsigned));

	#if defined(VCLONE) || defined(FCLONE)
	//free(child_stack);
	#endif

	return 0;
}

int child()
{
	usleep(100);
	(*licznik)++;
	_exit(0);
}