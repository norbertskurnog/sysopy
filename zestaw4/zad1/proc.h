#ifndef PROC_H
#define PROC_H

#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#include <stdio.h>
#include <errno.h>

/* Shared memory for variable licznik */
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */ 
#include <sys/wait.h>
#include <sys/times.h>

#include <string.h>

#if defined(VCLONE) || defined(FCLONE)
#include <sched.h>
#endif

#include <time.h>

unsigned* licznik;

int child();


#endif