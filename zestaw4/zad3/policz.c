#include "policz.h"

int main(int argc, char** argv)
{
	int verbose = 0;

	int test_mode = 0;
	int have_forked = 0;

	/* read command line */
	int i;
	for(i=1; i<argc; i++){
		if(!strcmp(argv[i], "-v") || !strcmp(argv[i], "--verbose")) verbose = 1;
		if(!strcmp(argv[i], "-w")) test_mode = 1;
	}

	int files_sum = 0;

	/* read env variables */
	char* path_to_browse = getenv("PATH_TO_BROWSE");
	if(path_to_browse == NULL || !path_to_browse[0]){
		printf("Environmental variable PATH_TO_BROWSE not set, setting to current directory\n");
		putenv("PATH_TO_BROWSE=.");
		path_to_browse=".";
	}

	int filter_by_ext = 0; /* wheater to check file nam extensions */
	char* ext_to_browse = getenv("EXT_TO_BROWSE");
	char** file_extensions = NULL;
	unsigned file_extensions_count = 0;

	if(ext_to_browse && ext_to_browse[0]){
		filter_by_ext = 1;

		/* count extensions */
		char* tmp = (char*)malloc(strlen(ext_to_browse)*sizeof(char)); /* strtok... */
		strcpy(tmp, ext_to_browse);

		char* tmp_buff = strtok(tmp, ":");
		while(tmp_buff){
			file_extensions_count++;
			tmp_buff = strtok(NULL, ":");
		}

		/* read again and split */
		strcpy(tmp, ext_to_browse);
		file_extensions = (char**)malloc(sizeof(char*)*file_extensions_count);
		int i;

		file_extensions[0] = strtok(tmp, ":");
		for(i=1; i<file_extensions_count; i++) {
			file_extensions[i] = strtok(NULL, ":");
		}

		//free(tmp);
	}
	
	DIR* dir;
	struct dirent* file_info;

	dir = opendir(path_to_browse);
	if(dir == NULL){
		printf("Cannot open directory %s\n", path_to_browse);
		exit(0);
	}

	while((file_info = readdir(dir))){

		/* for directory */
		if(file_info->d_type == DT_DIR && strcmp(file_info->d_name, ".") && strcmp(file_info->d_name, "..")){

			if(verbose) printf("Entering %s\n", file_info->d_name);
			pid_t pid = fork();

			if(pid == 0){
				/* in child */

				/* modyfing environmental variable */
				char buffer[255];
				sprintf(buffer, "%s/%s", path_to_browse, file_info->d_name);
				int error = setenv("PATH_TO_BROWSE", buffer, 1);
				int error_code = errno;

				if(error == -1){
					printf("Cannot set PATH_TO_BROWSE=%s\n", buffer);
					exit(error_code);
				}

				execv(argv[0], argv);
			}
			else if (pid > 0){
				/* in parent */
				have_forked = 1;
			}
		}
		/* for regular files */
		else if (file_info->d_type == DT_REG){

			if(filter_by_ext){
				char* last_ext = strtok(file_info->d_name, "."); /* name part, dont check */
				char* curr_ext = strtok(NULL, ".");

				while(curr_ext){
					last_ext = curr_ext;
					curr_ext = strtok(NULL, ".");
				}

				int i;
				for(i=0; i<file_extensions_count; i++){
					if(!strcmp(file_extensions[i], last_ext)){
						files_sum++;
						break;
					}
				}	
			}
			else files_sum++;
		}
	}

	/* wait for all children to terminate, read statuses and sum */
	unsigned children_sum = 0;

	if(test_mode && have_forked) sleep(15);

	int status;
	while(waitpid(-1, &status, 0)){
		if(errno == ECHILD) break;
			if(verbose) printf("Files in %s: %d\n", path_to_browse, WEXITSTATUS(status));
			if(verbose) printf("Files in %s and subdirectories: %d\n", path_to_browse, WEXITSTATUS(status)+files_sum);
			children_sum+=WEXITSTATUS(status);
	}

	/* check if processes parent is bash ;/ */
	char path_buf[255];
	sprintf(path_buf, "/proc/%d/cmdline", (unsigned)getppid());

	FILE* f = fopen(path_buf, "r");

	//in case proces name cannot be read, be verbose (or nothing will be printed)
	if(!f) verbose = 1; 

	else{
		fseek(f, 0, SEEK_END);
		int file_size = ftell(f);
		file_size = 100;
		rewind(f);

		char* file_buffer = (char*)malloc(sizeof(char)*(file_size+1));
		fread(file_buffer, file_size, 1, f);
		fclose(f);

		//obviosly, statement is true if strings are equal...
		if(!strcmp("/bin/bash", file_buffer))
		{
			printf("Files in directory %s and subdirectories: %d\n", path_to_browse, files_sum+children_sum);
			free(file_buffer);

			return 0;
		}

		free(file_buffer);
	} 

	free(file_extensions);

	if(test_mode && !have_forked) sleep(15);

	exit(files_sum+children_sum);
}