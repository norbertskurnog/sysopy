#ifndef ARCHIWIZUJ_H
#define ARCHIWIZUJ_H

#include <stdlib.h>
#include <stdio.h>

#include <errno.h>

/* stat */
#include <unistd.h> /* sleep */
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <time.h>
#include <string.h>

#include <dirent.h>
#endif