#include "dopisz.h"

int main(int argc, char** argv)
{
	/* read command line */
	if(argc < 4){
		printf("Usage: %s file period bytes\n", argv[0]);
		exit(1);
	}

	int period = atoi(argv[2]);
	int bytes = atoi(argv[3]);
	char* filename = argv[1];

	if(period < 1 || bytes < 1){
		printf("Period and bytes must by positive integers!\n");
		exit(1);
	}

	/* open destination file */
	FILE* file = fopen(filename, "w");
	if(!file){
		printf("Cannot open file %s\n", filename);
		exit(errno);
	}

	time_t curr_time;
	char* datetime;

	unsigned pid = (unsigned)getpid();

	srand(time(NULL));
	char* random_bytes = (char*)malloc(bytes);

	/* main loop */
	while(1){
		curr_time = time(NULL);
		datetime = ctime(&curr_time);

		int i;
		for(i=0; i<bytes; i++) random_bytes[i]='A' + (rand() % 26);

		fprintf(file, "pid: %d, date: %s%s\n", pid, datetime, random_bytes);
		fflush(file);

		sleep(period);
	}

	return 1;
}