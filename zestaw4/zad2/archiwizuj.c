#include "archiwizuj.h"

int main(int argc, char** argv)
{
	int error_code;

	if(argc<3){
		printf("Usage: %s file period limit\n", argv[0]);
		exit(1);
	}

	unsigned period = atoi(argv[2]);
	unsigned limit = atoi(argv[3]);

	if(period<1 || limit<1){
		printf("limit and period must be > 0\n");
		exit(2);
	}

	struct stat s;

	if(lstat(argv[1], &s) < 0){
		error_code = errno;
		printf("Cannot stat file %s\n", argv[1]);
		exit(error_code);
	}

	/* if 'archiwum' is not present, create it */
	DIR* dir = opendir("archiwum");
	if(dir == NULL){
		system("mkdir archiwum");
	}
	free(dir);
	dir = NULL;

	int start_date = s.st_mtime;

	time_t raw_time;
	struct tm* t;
	char* new_filename;


	/* monitor */
	while(1){
		if(lstat(argv[1], &s) < 0){
			int error_code = errno;
			printf("Cannon stat file %s\n", argv[1]);
			exit(error_code);
		}

		if(s.st_mtime != start_date && s.st_size > limit){
			pid_t pid = fork();

			if(pid == 0){
				/* child */
				time(&raw_time);
				t = localtime(&raw_time);
				new_filename = (char*)malloc(30+strlen(argv[1]));

				sprintf(new_filename, "archiwum/%s_%d-%d-%d_%d-%d-%d", argv[1], t->tm_year+1900, t->tm_mon+1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);

				execlp("cp", "cp", argv[1], new_filename, NULL);
			}
			else if(pid > 0)
			{
				/*in parent*/
				/* wait for sigchld, truncate file... */
				int status;
				waitpid(pid, &status, 0); 
				FILE* f = fopen(argv[1], "w"); 
				fclose(f);
			}
			else if (pid < 0){
				error_code = errno;
				printf("Fork error!\n");
				exit(error_code);
			}

			exit(0);
		}

		sleep(period);
	}

	return 0;
}

