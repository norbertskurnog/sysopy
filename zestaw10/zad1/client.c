#include "common.h"
#include <pthread.h>

void usage_note();
void get_client_id(char* id);
void print_client_list();

void* socket_listener(void* args);
int block_listener = 0;
pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;

int server_type;
int client_id_no = -1;

char* local_socket_path;
int remote_port;

/* server socket desctiptor */
int server_socket;
struct sockaddr* addr;
socklen_t addrlen;

struct sockaddr* addr_rec;
socklen_t addrlen_rec;

request_t request;

int main(int argc, char *argv[])
{
	/* parse input arguments */
	if(argc < 3){
		usage_note(argv[0]);
	}

	if(!strcmp(argv[1], "local")){
		if(argc < 3){
			usage_note(argv[0]);
			exit(1);
		}

		server_socket = socket(AF_LOCAL, SOCK_DGRAM, 0);
		if(server_socket < 0){
			perror("socket");
			exit(errno);
		}

		/* connection info */
		struct sockaddr_un local_addr;

		local_addr.sun_family = AF_UNIX;
		strcpy(local_addr.sun_path, argv[2]);
		addr = (struct sockaddr*)&local_addr;
		addrlen = sizeof(struct sockaddr_un);

		int optval = 1;
		setsockopt(server_socket, SOL_SOCKET, SO_PASSCRED, &optval, sizeof(optval));
	}
	else if(!strcmp(argv[1], "remote")){
		if(argc < 4){
			usage_note(argv[0]);
			exit(1);
		}

		server_socket = socket(AF_INET, SOCK_DGRAM, 0);
		if(server_socket < 0){
			perror("socket");
			exit(errno);
		}

		/* bind socket to address */
		struct sockaddr_in remote_addr;
		memset(&remote_addr, 0, sizeof(struct sockaddr_in));

		remote_addr.sin_family = AF_INET;

		/* port */
		if(sscanf(argv[3], "%d", &remote_port) < 1){
			printf("port must be positive integer\n");
			exit(1);
		}
		remote_addr.sin_port = htons(remote_port);

		/* domain */
		if(inet_pton(AF_INET, argv[2], &remote_addr.sin_addr) < 1){
			printf("\"%s\" is not valid ipv4 address\n", argv[2]);
			exit(1);
		}

		addr = (struct sockaddr*)&remote_addr;
		addrlen = sizeof(struct sockaddr_in);
	}
	else{
		usage_note(argv[0]);
		exit(1);
	}

	struct sockaddr_un me;
	me.sun_family = AF_UNIX;
	bind(server_socket, (void*)&me, sizeof(short));
	
	/* send registration request */
	printf("Trying to register at server...\n");

	request.action = REQ_REGISTER;
	
	char* client_id = (char*)malloc(HOST_NAME_MAX + 64); //zwolnic to gunwno na koncu
	get_client_id(client_id);
	strcpy(request.name, client_id);

	sendto(server_socket, &request, sizeof(request_t), 0, addr, addrlen);

	/* receive registration ack */
	recvfrom(server_socket, &request, sizeof(request_t), 0, addr, &addrlen);
	client_id_no = request.id;
	printf("Registered at server as %s\n", client_id);

	/* receive and print list received from server */

	pthread_t listener_th;
	pthread_create(&listener_th, NULL, socket_listener, NULL);


	/* main loop */
	int choice;
	while(1){

		/* read commands from user */
		printf("\n1. List connected clients\n2. Send command to client\n3. Disconnect and exit\n:");
		scanf("%d", &choice);
		printf("\n");

		switch(choice){
			case 1:{
				request.action = REQ_CLIENT_LIST;
				request.id = client_id_no;
				sendto(server_socket, &request, sizeof(request_t), 0, addr, addrlen);

				break;
			}
			case 2:{
				printf("\n\nCommand to execute:\n");
				scanf("%s", request.data);
				printf("Client to execute command:\n");
				scanf("%d", &request.id_dest);

				request.action = REQ_COMMAND;
				request.id = client_id_no;
				sendto(server_socket, &request, sizeof(request_t), 0, addr, addrlen);

				printf("Waiting for result...\n");
				fflush(stdout);

				break;
			}
			case 3:{
				request.action = REQ_UNREGISTER;
				request.id = client_id_no;
				sendto(server_socket, &request, sizeof(request), 0, addr, addrlen);

				printf("Disconnected.\n");
				raise(SIGINT);

				break;
			}
			default:{
				printf("Unrecognized option\n");
				break;
			}
		}
	}

	return 0;
}

void usage_note(char* program_name){
	printf("Usage: %s local|remote args\n\
	local: socket_path\n\
	remote: ip_address port\n", program_name);
	exit(1);
}

void get_client_id(char* id){
	char* tmp = (char*)malloc(HOST_NAME_MAX + 64);

	gethostname(tmp, sizeof(tmp) - 1);
	strcpy(id, tmp);
	strcat(id, ".");
	getdomainname(tmp, sizeof(tmp) - 1);
	strcat(id, tmp);

	free(tmp);
}

void print_client_list(){
	client_list_t client_list;
	int receiving = 1;

	printf("List of clients registered on server:\n");

	int clients_sum = 0;
	while(receiving){
		recvfrom(server_socket, &client_list, sizeof(client_list_t), 0, addr, &addrlen);
		
		/* end of transsmision signal */
		if(client_list.client_count == -1) receiving = 0;

		/* print received part of list */
		int i;
		for(i=0; i<client_list.client_count; i++) printf("%d. %s\n", i+clients_sum+1, client_list.clients[i]);
		
		clients_sum += client_list.client_count;
	}
	printf("Total %d.\n", clients_sum);

	return;
}

void* socket_listener(void* args)
{
	printf("Listener started\n");
	while(1){

		/* receive commands from server */
		recvfrom(server_socket, &request, sizeof(request), 0, addr, &addrlen);

		if(request.action == REPLY_CLIENT_LIST){
			print_client_list();
		}
		else if(request.action == REQ_COMMAND){
			request.action = REPLY_COMMAND;
			request.id_dest = request.id;
			printf("Request from %d: \"%s\"\n", request.id, request.data);
			fflush(stdout);

			FILE* fp = popen(request.data, "r");
			int len = read(fileno(fp), request.data, MAX_DATA_LENGTH);
			request.data[len] = '\0';
			pclose(fp);

			sendto(server_socket, &request, sizeof(request_t), 0, addr, addrlen);
		}
		else if (request.action == REPLY_COMMAND){
			printf("%s", request.data);
			fflush(stdout);
		}
		else{
			printf(".");
			fflush(stdout);
		}

	}

	return (void*)0;
}