#ifndef COMMON_H
#define COMMON_H

#include <sys/types.h>
#include <sys/socket.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include <limits.h>
#include <netdb.h>
#include <signal.h>


#define CLIENT_NAME_LENGTH 32
#define MAX_DATA_LENGTH 128

typedef enum {
	/* queries to server */
	REQ_REGISTER,
	REQ_CLIENT_LIST,
	REQ_COMMAND,
	REQ_UNREGISTER,

	/* replies to client */
	REPLY_CLIENT_LIST,
	REPLY_COMMAND,
	REPLY_REGISTERED
} action_t;

typedef struct {
	char name[CLIENT_NAME_LENGTH]; //redundant ;/
	int id;
	action_t action;

	int id_dest;
	char data[MAX_DATA_LENGTH];
} request_t;

typedef struct {
	int client_count;
	char clients[8][HOST_NAME_MAX + 64];
} client_list_t;

#endif