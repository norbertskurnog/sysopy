#include "common.h"

#define MAX_CLIENTS 25

char* local_socket_path;
int remote_socket_port;

int local_socket, remote_socket;

struct client_t {
	char *name;
	int id;
	struct sockaddr *addr;
	socklen_t addrlen;
	int used;
} clients[MAX_CLIENTS];

int connected_clients = 0;

struct client_t* register_client(char* name, struct sockaddr *src_addr, socklen_t addrlen);
void unregister_client(struct client_t* client);
struct client_t* find_client_by_id(int id);

int main(int argc, char *argv[])
{
	/* parse input arguments */
	if(argc < 2){
		printf("Usage: %s path port\n", argv[0]);
		exit(1);
	}
	
	local_socket_path = argv[1];
	if(sscanf(argv[2], "%d", &remote_socket_port) < 1 || remote_socket_port < 0){
		printf("port must be an positive integer!\n");
		exit(1);
	}

	/* open and bind local socket */
	local_socket = socket(AF_LOCAL, SOCK_DGRAM, 0);
	
	struct sockaddr_un addr_local;
	addr_local.sun_family = AF_UNIX;
	strcpy((char*)&addr_local.sun_path, argv[1]);

	if(bind(local_socket, (struct sockaddr*)&addr_local, sizeof(struct sockaddr_un)) == -1){
		perror("bind local");
		exit(errno);
	}

	/* open and bind remote socket */
	remote_socket = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in addr_remote;
	addr_remote.sin_family = AF_INET;
	addr_remote.sin_port = htons(remote_socket_port);
	inet_pton(AF_INET, "127.0.0.1", &addr_remote.sin_addr);

	if(bind(remote_socket, (struct sockaddr*)&addr_remote, sizeof(struct sockaddr_in)) == -1){
		perror("bind local");
		exit(errno);
	}

	/* start receiving messages */
	request_t request;

	struct sockaddr_in src_addr_remote;
	socklen_t src_addr_remote_len = sizeof(struct sockaddr_in);
	struct sockaddr_un src_addr_local;
	socklen_t src_addr_local_len = sizeof(struct sockaddr_un);

	/* socket managment */
	struct sockaddr *src_addr;
	socklen_t addrlen;
	int socket_reply;

	/* which client was message from */
	struct client_t* src_client;

	while (1) {
		while(1){
			if(recvfrom(remote_socket, &request, sizeof(request_t), MSG_DONTWAIT, (struct sockaddr*)&src_addr_remote, &src_addr_remote_len) > 0){
				src_addr = (struct sockaddr*)&src_addr_remote;
				addrlen = src_addr_remote_len;
				socket_reply = remote_socket;
				break;
			}

			if(recvfrom(local_socket, &request, sizeof(request_t), MSG_DONTWAIT, (struct sockaddr*)&src_addr_local, &src_addr_local_len) > 0){
				src_addr = (struct sockaddr*)&src_addr_local;
				addrlen = src_addr_local_len;
				socket_reply = local_socket;
				break;
			}
		}

		if (request.action == REQ_REGISTER) {
			if ((src_client = register_client(request.name, src_addr, addrlen)) != NULL){
				request.action = REPLY_REGISTERED;
				request.id_dest = src_client->id;
				request.id = src_client->id;
				sendto(socket_reply, &request, sizeof(request_t), 0, src_addr, addrlen);

				request.action = REQ_CLIENT_LIST;
			}
			else continue;
		}
		else{
			if ((src_client = find_client_by_id(request.id)) == NULL) {
				free(src_addr); /* client not found */
				printf("Client with id=%d not registered\n", request.id);
				continue;
			}
		}
		if (request.action == REQ_CLIENT_LIST) {
			printf("Client %d requested clinets list\n", request.id);
			
			/* start sending client list split into packets*/
			request.action = REPLY_CLIENT_LIST;
			request.id_dest = request.id;
			sendto(socket_reply, &request, sizeof(request_t), 0, src_addr, addrlen);

			client_list_t list_response;
			
			int i, j, clients_count=0;
			for(i=0; i<MAX_CLIENTS; i++){
				for(j=0; j<8; j++, i++){
					if(clients[i].used){
						strcpy(list_response.clients[j], clients[i].name);
						clients_count++;
					}
				}
				list_response.client_count = clients_count;
				clients_count = 0;
				sendto(socket_reply, &list_response, sizeof(client_list_t), 0, src_addr, addrlen);

			}

			list_response.client_count = -1;
			sendto(socket_reply, &list_response, sizeof(client_list_t), 0, src_addr, addrlen);
		}
		else if (request.action == REQ_UNREGISTER) {
			unregister_client(src_client);
			printf("Client %d disconnected\n", request.id);
		}
		else if (request.action == REQ_COMMAND){
			printf("Request from %d for %d\n", request.id, request.id_dest);
			struct client_t* dst_client = find_client_by_id(request.id_dest);
			if(!dst_client){
				printf("Client %d does not exist!\n", request.id_dest);
				fflush(stdout);
				continue;
			}

			sendto(socket_reply, &request, sizeof(request), 0, dst_client->addr, dst_client->addrlen);
		}
		else if (request.action == REPLY_COMMAND){
			struct client_t* dst_client = find_client_by_id(request.id_dest);
			if(!dst_client){
				printf("Client %d does not exist!\n", request.id_dest);
				fflush(stdout);
				continue;
			}

			sendto(socket_reply, &request, sizeof(request), 0, dst_client->addr, dst_client->addrlen);
		}
		else{
			printf("Unrecognized message type.\n");
		}
	}	

	return 0;
}

struct client_t* register_client(char* name, struct sockaddr *src_addr, socklen_t addrlen) {
	int i;
	for (i = 0; i < MAX_CLIENTS && clients[i].used; i++);
	if (i == MAX_CLIENTS) return NULL; /* list is full */
	
	clients[i].name = calloc(strlen(name) + 1, sizeof(char));
	strcpy(clients[i].name, name);
	clients[i].id = i;
	clients[i].addr = malloc(sizeof(struct sockaddr));
	memcpy(clients[i].addr, src_addr, sizeof(struct sockaddr));
	clients[i].addrlen = addrlen;
	clients[i].used = 1;

	connected_clients++;

	return &(clients[i]);
}

void unregister_client(struct client_t* client) {
	free(client->name);
	free(client->addr);
	client->used = 0;
	connected_clients--;
}

struct client_t* find_client_by_id(int id){
	int i;
	for(i=0; i<MAX_CLIENTS && clients[i].id != id; i++);
	if(i == MAX_CLIENTS) return NULL;

	return &(clients[i]);
}