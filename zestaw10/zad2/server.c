#include "common.h"

#define MAX_CLIENTS 25

void* client_thread(void* params);

char* local_socket_path;
int remote_socket_port;

int local_socket, remote_socket;

struct client_t {
	char *name;
	int id;
	int socket_desc;
	struct sockaddr *addr;
	socklen_t addrlen;
	int used;
} clients[MAX_CLIENTS];

int connected_clients = 0;

struct client_t* register_client(char* name, int socket_desc, struct sockaddr *src_addr, socklen_t addrlen);
void unregister_client(struct client_t* client);
struct client_t* find_client_by_id(int id);

int main(int argc, char *argv[])
{
	/* parse input arguments */
	if(argc < 2){
		printf("Usage: %s path port\n", argv[0]);
		exit(1);
	}
	
	local_socket_path = argv[1];
	if(sscanf(argv[2], "%d", &remote_socket_port) < 1 || remote_socket_port < 0){
		printf("port must be an positive integer!\n");
		exit(1);
	}

	/* open and bind local socket */
	local_socket = socket(AF_LOCAL, SOCK_STREAM, 0);
	fcntl(local_socket, F_SETFL, O_NONBLOCK);
	
	struct sockaddr_un addr_local;
	addr_local.sun_family = AF_UNIX;
	strcpy((char*)&addr_local.sun_path, argv[1]);

	if(bind(local_socket, (struct sockaddr*)&addr_local, sizeof(struct sockaddr_un)) == -1){
		perror("bind local");
		exit(errno);
	}

	listen(local_socket, MAX_CLIENTS);

	/* open and bind remote socket */
	remote_socket = socket(AF_INET, SOCK_STREAM, 0);
	fcntl(remote_socket, F_SETFL, O_NONBLOCK);

	struct sockaddr_in addr_remote;
	addr_remote.sin_family = AF_INET;
	addr_remote.sin_port = htons(remote_socket_port);
	inet_pton(AF_INET, "127.0.0.1", &addr_remote.sin_addr);

	if(bind(remote_socket, (struct sockaddr*)&addr_remote, sizeof(struct sockaddr_in)) == -1){
		perror("bind local");
		exit(errno);
	}

	listen(remote_socket, MAX_CLIENTS);

	/* start receiving messages */
	request_t request;

	/* socket managment */
	struct sockaddr *src_addr = malloc(sizeof(struct sockaddr));
	socklen_t addrlen;

	/* which client was message from */
	struct client_t* src_client;

	while (1) {
		/* accept pending client */
		int client_sock_desc;
		while(1){
			if((client_sock_desc = accept(local_socket, src_addr, &addrlen)) > 0) break;
			if((client_sock_desc = accept(remote_socket, src_addr, &addrlen)) > 0) break;
		}
		printf("New client connected.\n");

		/* register new client */ //da sie jakiegos sleepa i ponowienie proby ale to przy refactoringu
		if(recv(client_sock_desc, &request, sizeof(request_t), 0) > 0){
			if (request.action == REQ_REGISTER) {
				if ((src_client = register_client(request.name, client_sock_desc, src_addr, addrlen)) != NULL){
					pthread_t client_th;
					pthread_create(&client_th, NULL, client_thread, src_client);
				}
				else{
					printf("Cannot register client \"%s\"\n", request.name);
				}
			}
			else{
				printf("Unexpected message type.\n");
			}
		}
		else{
			printf("Client not responding\n");
		}
	}
	return 0;
}

struct client_t* register_client(char* name, int socket_desc, struct sockaddr *src_addr, socklen_t addrlen) {
	int i;
	for (i = 0; i < MAX_CLIENTS && clients[i].used; i++);
	if (i == MAX_CLIENTS) return NULL; /* list is full */
	
	clients[i].name = calloc(strlen(name) + 1, sizeof(char));
	strcpy(clients[i].name, name);
	clients[i].id = i;
	clients[i].socket_desc = socket_desc;
	clients[i].addr = malloc(sizeof(struct sockaddr));
	memcpy(clients[i].addr, src_addr, sizeof(struct sockaddr));
	clients[i].addrlen = addrlen;
	clients[i].used = 1;

	connected_clients++;

	return &(clients[i]);
}

void unregister_client(struct client_t* client) {
	free(client->name);
	free(client->addr);
	client->used = 0;
	connected_clients--;
}

struct client_t* find_client_by_id(int id){
	int i;
	for(i=0; i<MAX_CLIENTS && clients[i].id != id; i++);
	if(i == MAX_CLIENTS) return NULL;

	return &(clients[i]);
}

void* client_thread(void* params){

	request_t request;
	struct client_t client;
	memcpy(&client, params, sizeof(struct client_t));

	printf("Client %s registered.\n", client.name);

	/* send confirmation of registration */
	request.action = REPLY_REGISTERED;
	request.id_dest = client.id;
	request.id = client.id;
	strcpy(request.name, client.name);
	send(client.socket_desc, &request, sizeof(request_t), 0);

	while(1){
		if(recv(client.socket_desc, &request, sizeof(request_t), 0) < 0){
			printf("Error receivin message from client\n");
		}

		if(request.action == REQ_CLIENT_LIST) {
			printf("Client %d requested clinets list\n", request.id);
			
			/* start sending client list split into packets*/
			request.action = REPLY_CLIENT_LIST;
			send(client.socket_desc, &request, sizeof(request_t), 0);

			client_list_t list_response;
			
			int i, j, clients_count=0;
			for(i=0; i<MAX_CLIENTS; i++){
				for(j=0; j<8; j++, i++){
					if(clients[i].used){
						strcpy(list_response.clients[j], clients[i].name);
						clients_count++;
					}
				}
				list_response.client_count = clients_count;
				clients_count = 0;
				send(client.socket_desc, &list_response, sizeof(client_list_t), 0);

			}

			list_response.client_count = -1;
			send(client.socket_desc, &list_response, sizeof(client_list_t), 0);
		}
		else if (request.action == REQ_UNREGISTER) {
			unregister_client(&client);
			printf("Client %d disconnected\n", request.id);
			pthread_exit(0);
		}
		else if (request.action == REQ_COMMAND){
			printf("Request from %d for %d\n", request.id, request.id_dest);
			struct client_t* dst_client = find_client_by_id(request.id_dest);
			if(!dst_client){
				printf("Client %d does not exist!\n", request.id_dest);
				fflush(stdout);
				continue;
			}
			send(dst_client->socket_desc, &request, sizeof(request), 0);
		}
		else if (request.action == REPLY_COMMAND){
			struct client_t* dst_client = find_client_by_id(request.id);
			if(!dst_client){
				printf("Client %d does not exist!\n", request.id_dest);
				fflush(stdout);
				continue;
			}

			send(dst_client->socket_desc, &request, sizeof(request), 0);
		}
		else{
			printf("Unrecognized message type.\n");
		}
	}

	return (void*)0;
}