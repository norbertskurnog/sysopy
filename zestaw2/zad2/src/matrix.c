#include "matrix.h"

#include <libmemory/memory.h>

//#include "config.h"
//wczytaj macierz

void matrix_init(size_t size)
{
	init(size);
}

MATRIX read_matrix(FILE* file)
{
	/*Format pliku: linia zawierajaca dwie liczby, kolejno liczba wierszy i kolumn
	  nastepnie w kolejnych wierszach podane kolejne wiersze macierzy 
	  Na koncu pliku musi wystapic pusta linia!*/

	int i,j; //liczniki do petli

	//wczytanie rozmiaru pierwszej macierzy
	int rows, cols;
	rows = (int)read_number(file);
	cols = (int)read_number(file);

	//stworzenie macierzy
	real** values = alloc(rows*sizeof(real*));
	for(i=0; i<rows; i++)
	{
		values[i] = alloc(cols*sizeof(real));
	}
 
	//wypelnienie macierzy
	for(i=0; i<rows; i++)
		for(j=0; j<cols; j++)
			values[i][j]=read_number(file);

	MATRIX matrix;
	matrix.values = values;
	matrix.rows=rows;
	matrix.cols=cols;

	return matrix;
}

//wypisuje macierz podana jako argument na ekran
void print_matrix(MATRIX* matrix)
{
	printf("rows %d, cols %d", matrix->rows, matrix->cols);
	int i,j;
	for(i=0; i<matrix->rows; i++)
	{
		printf("\n");
		for(j=0; j<matrix->cols; j++)
		{
			printf("%3.3f ", matrix->values[i][j]);
		}
	}
}

//wczytaj kolejna liczbe z pliku
real read_number(FILE* file)
{
	char file_char = (char)fgetc(file); //znak wczytany z pliku

	char number[10]; //bufor na znaki ktore beda przekonwertowne na liczbe
	memset(number, 0, 10*sizeof(char));

	//czytanie kolejnych znakow az do spacji lub konca linii
	int i;
	for(i=0; file_char != ' ' && file_char != '\n'; i++)
	{
		number[i] = file_char;
		file_char = (char)fgetc(file);

	}

	return (real)atof(number);
}

int multiply_matrices(MATRIX* matrix1, MATRIX* matrix2, MATRIX* matrix_res)
{
	if(matrix1->cols != matrix2->rows){
		printf("\nNumber of columns in matrix 1 is different than number of rows in matrix 2!\nCannot multiply. \n");
		return 1;
	}

	int i; 

	//tworzenie macierzy wynikowej
	matrix_res->rows = matrix1->rows;
	matrix_res->cols = matrix2->cols;

	matrix_res->values = alloc(matrix_res->rows*sizeof(real*));
	for(i=0; i<matrix_res->rows; i++)
		matrix_res->values[i] = alloc(matrix_res->cols*sizeof(real));

	int row, col; //aktualnie przetwarzany wiersz i kolumna
	for(row=0; row<matrix1->rows; row++){
		for(col=0; col<matrix2->cols; col++){
			real tmp=0;
			for(i=0; i<matrix1->cols; i++){
				tmp+=matrix1->values[row][i]*matrix2->values[i][col];
			}
			matrix_res->values[row][col]=tmp;
		}
	}

	return 0;
}

int add_matrices(MATRIX* matrix1, MATRIX* matrix2, MATRIX* matrix_res)
{
	if(matrix1->cols != matrix2->cols || matrix1->rows != matrix2->rows){
		printf("\nCannot add matrices of different dimmensions!\n");
		return 1;
	}

	int i, j;

	//tworzenie macierzy wynikowej
	matrix_res->rows = matrix1->rows;
	matrix_res->cols = matrix1->cols;

	matrix_res->values = alloc(matrix_res->rows*sizeof(real*));
	for(i=0; i<matrix_res->rows; i++)
		matrix_res->values[i] = alloc(matrix_res->cols*sizeof(real));

	for(i=0; i<matrix_res->rows; i++)
		for(j=0; j<matrix_res->cols; j++)
			matrix_res->values[i][j] = matrix1->values[i][j] + matrix2->values[i][j];

	return 0;
}