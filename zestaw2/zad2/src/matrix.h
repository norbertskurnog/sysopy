#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

#define real float

#ifdef DOUBLE
	#undef real
	#define real double
#endif

typedef struct Matrix
{
	real** values;
	int rows;
	int cols;
}MATRIX;

void matrix_init(size_t size);

real read_number(FILE* file);
MATRIX read_matrix(FILE* file);
void print_matrix(MATRIX* matrix);

int multiply_matrices(MATRIX* matrix1, MATRIX* matrix2, MATRIX* matrix_res);
int add_matrices(MATRIX* matrix1, MATRIX* matrix2, MATRIX* matrix_res);

#endif