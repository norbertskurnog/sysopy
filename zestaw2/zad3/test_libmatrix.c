#include "include/matrix.h"
#include "include/memory.h"

#include <unistd.h>
#include <sys/times.h>
#include <time.h>


void print_memory_info();
void pr_times(clock_t, struct tms *, struct tms *);

int main(int argc, char* argv[])
{


	matrix_init(1024);

	clock_t start_time, chp1, chp2, end_time;
	struct tms tms_start, tms_chp1, tms_chp2, tms_end;

	start_time = clock();
	times(&tms_start);
	printf("Program begin\n");
	pr_times(0, &tms_start, &tms_start);
	print_memory_info();
	

	if (argc<3)
	{
		printf("Usage: %s file mul|add\n", argv[0]);
		return 1;
	}

	int operation = 0;
	if(strcmp(argv[2], "mul") == 0) operation = 1;
	else if(strcmp(argv[2], "add") == 0) operation = 2;
	else{
		printf("Usage: %s file mul|add\n", argv[0]);
		return 3;
	}

	//otwarcie pliku
	FILE* f = fopen(argv[1], "r");

	if(f==0)
	{
		printf("Could not open file %s\n", argv[1]);
		return 2;
	}

	chp1 = clock();
	times(&tms_chp1);
	printf("\nAfter opening file\n");
	pr_times(chp1-start_time, &tms_start, &tms_chp1);
	print_memory_info();

	MATRIX matrix1 = read_matrix(f);
	MATRIX matrix2 = read_matrix(f);
	printf("Matrix 1\n");
	print_matrix(&matrix1);
	printf("\nMatrix 2\n");
	print_matrix(&matrix2);

	chp2 = clock();
	times(&tms_chp2);
	printf("\nAfter reading file\n");
	pr_times(chp2-start_time, &tms_start, &tms_chp2);
	printf("current - last\n");
	pr_times(chp2-chp1, &tms_chp1, &tms_chp2);
	print_memory_info();
	
	MATRIX matrix3;

	int error;
	if(operation == 1){
		error = multiply_matrices(&matrix1, &matrix2, &matrix3);
		if(!error) printf("\nmultiplied:\n");
	}
	else if(operation == 2){
		error = add_matrices(&matrix1, &matrix2, &matrix3);
		if(!error) printf("\nadded:\n");
	}

	if(!error) print_matrix(&matrix3);

	end_time = clock();
	times(&tms_end);
	printf("\nProgram end (after add/multiply operation)\n");
	pr_times(end_time-start_time, &tms_start, &tms_end);
	printf("current - last\n");
	pr_times(end_time-chp2, &tms_chp2, &tms_end);
	print_memory_info();
			
	return 0;
}

void print_memory_info()
{
	memory_info_t* minfo = meminfo();
	printf("Memory info: allocated areas: %d, free areas: %d, smallest free area: %d, biggest free area: %d\n", 
		minfo->allocated_areas, minfo->free_areas, minfo->smallest_area, minfo->biggest_area);
}

void pr_times(clock_t real_t, struct tms *tms_start, struct tms *tms_end)
{
    long clktck = CLOCKS_PER_SEC;

    printf(" real:  %7.5fs\n", real_t/(float)clktck);
    printf(" user:  %7.5fs\n", (tms_end->tms_utime - tms_start->tms_utime)/(float)clktck);
    printf(" sys:   %7.5fs\n", (tms_end->tms_stime - tms_end->tms_stime)/(float)clktck);

}