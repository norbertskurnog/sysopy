
#include <unistd.h>
#include <sys/times.h>
#include <time.h>

#include <stdio.h>

#include "include/memory.h"

void print_memory_info();
void pr_times(clock_t, struct tms *, struct tms *);

int main()
{
	if(!init(1024)) return 1;

	clock_t start_time, chp1, chp2, end_time;
	struct tms tms_start, tms_chp1, tms_chp2, tms_end;

	start_time = clock();
	times(&tms_start);
	printf("Program begin\n");
	pr_times(0, &tms_start, &tms_start);
	print_memory_info();
	
	//allocation test
	void* area1 = alloc(5);
	void* area2 = alloc(5);
	void* area3 = alloc(5);
	void* area4 = alloc(5);
	void* area5 = alloc(5);

	chp1 = clock();
	times(&tms_chp1);
	printf("\nAfter allocation\n");
	pr_times(chp1-start_time, &tms_start, &tms_chp1);
	print_memory_info();

	dealloc(area3);
	dealloc(area4);
	dealloc(area5);

	chp2 = clock();
	times(&tms_chp2);
	printf("\nAfter deallocation\n");
	pr_times(chp2-start_time, &tms_start, &tms_chp2);
	printf("current - last\n");
	pr_times(chp2-chp1, &tms_chp1, &tms_chp2);
	print_memory_info();

	defragment();

	end_time = clock();
	times(&tms_end);
	printf("\nProgram end (after defragmentation)\n");
	pr_times(end_time-start_time, &tms_start, &tms_end);
	printf("current - last\n");
	pr_times(end_time-chp2, &tms_chp2, &tms_end);
	print_memory_info();

	return 0;
}

void print_memory_info()
{
	memory_info_t* minfo = meminfo();
	printf("Memory info: allocated areas: %d, free areas: %d, smallest free area: %d, biggest free area: %d\n", 
		minfo->allocated_areas, minfo->free_areas, minfo->smallest_area, minfo->biggest_area);
}

void pr_times(clock_t real, struct tms *tms_start, struct tms *tms_end)
{
    long clktck = CLOCKS_PER_SEC;

    printf(" real:  %7.5fs\n", real/(float)clktck);
    printf(" user:  %7.5fs\n", (tms_end->tms_utime - tms_start->tms_utime)/(float)clktck);
    printf(" sys:   %7.5fs\n", (tms_end->tms_stime - tms_end->tms_stime)/(float)clktck);

}