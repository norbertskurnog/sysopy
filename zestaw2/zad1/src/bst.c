#include "bst.h"

#include <stdlib.h>
#include <stdio.h>

/*Wszystkie funkcje w przypadku niepowodzenia zwracaja NULL */

node_t* bst_insert(node_t* root, void* addr, size_t size)
{
	//alokacja pamieci na nowa strukture do drzewa
	node_t* node = malloc(sizeof(node_t));
	node->addr = addr;
	node->size = size;
	node->left_child = NULL;
	node->right_child = NULL;
	node->parent = NULL;

	if(root == NULL){
		return node;
	}

	//wstawianie
	node_t* current_node = root;

	while(1){
		if(node->size < current_node->size){
			if(current_node->left_child == NULL){
				node->parent = current_node;
				current_node->left_child = node;
				return root;
			}
			current_node = current_node->left_child;
		}
		else{
			if(current_node->right_child == NULL){
				node->parent = current_node;
				current_node->right_child = node;
				return root;
			}
			current_node = current_node->right_child;
		}
	}
}

node_t* bst_search_min(node_t* root, size_t size)
{
	//znajduje najmniejszy element >= size

	if (root == NULL) return NULL;

	node_t* current_node = root;

	if(size == 0){
		while(current_node->left_child != NULL)
			current_node = current_node->left_child;
		return current_node;
	}

	node_t* smallest_area = bst_search_max(root);

	while(1){
		//nawet jesli rozmiar znalezionego bloku jest za maly, to i tak zwroc (odpowiednie sprawdzenie dokonuje sie na poziomie biblioteki)
		if(current_node == NULL) return smallest_area;

		if(current_node->size == size) return current_node;
		else if(current_node->size > size){
			if(current_node->size < smallest_area->size) smallest_area = current_node;
			current_node = current_node -> left_child;
		}
		else if(current_node->size < size){
			current_node = current_node->right_child;
		}
	}
}

node_t* bst_search_max(node_t* root)
{
	if (root == NULL) return NULL;

	node_t* current_node = root;

	while(current_node->right_child != NULL)
		current_node = current_node->right_child;

	return current_node;
}

node_t* bst_succesor(node_t* root, node_t* node)
{
	if (root == NULL) return NULL;

	node_t* current_node = node;

	if(node->right_child != NULL){
		current_node = node->right_child;
		while(current_node->left_child != NULL){
			current_node = current_node->left_child;
		}
		return current_node;
	}
	else{
		current_node = node;
		while(1){
			if(current_node->parent == NULL) return NULL; //nie ma nastepnika, najwiekszy element drzewa

			if(current_node->parent->left_child == current_node){
				return current_node->parent;
			}
			else{
				current_node = current_node->parent;
			}
		}
	}
}

node_t* bst_delete(node_t* root, node_t* node)
{
	if (root == NULL) return NULL;

	//domyslne wartosci do zwrocenia/usuniecia
	node_t* node_delete = node;
	node_t* node_return = root;

	//usuwamy korzen
	if(node->parent == NULL){
		free(root);
		return NULL;
	}

	//nie ma synow
	if(node->left_child == NULL && node->right_child == NULL){
		if(node->parent->left_child == node) node->parent->left_child=NULL;
		else node->parent->right_child = NULL;

		//drzewo zawiera jeden element, ktorym jest korzen
		if(node->parent == NULL) node_return = NULL;
	}
	//ma jednego syna
	else if(node->left_child == NULL || node->right_child == NULL)
	{
		//o ktorego syna dokladnie chodzi
		node_t* child;
		if(node->left_child != NULL) child = node->left_child;
		else child = node->right_child;

		//usuwanie korzenia
		if(node->parent == NULL) node_return = child;
		
		if(node->parent->left_child == node){
			node->parent->left_child=child;
		} 
		else node->parent->right_child = child;

		//free(node);
		return root;
	}
	else{
		node_t* succesor = bst_succesor(root, node);

		node->size = succesor->size;
		node->addr = succesor->addr;

		bst_delete(root, succesor);

		node_delete = NULL;
	}

	free(node_delete);
	return node_return;
}

size_t bst_elements_count(node_t* node)
{
	if(node == NULL) return 0;

	size_t left_count = bst_elements_count(node->left_child);
	size_t right_count = bst_elements_count(node->right_child);
	return left_count + right_count + 1;
}

node_t* in_order(node_t* node, node_t** array, int* i)
{
	if (node == NULL) return NULL;

	if(node->left_child != NULL) in_order(node->left_child, array, i);
	array[*i] = node;
	*i=*i+1;
	if(node->right_child != NULL) in_order(node->right_child, array, i); 

	return node;
}

node_t** bst_sorted_array(node_t* root)
{
	if (root == NULL) return NULL;

	size_t elements = bst_elements_count(root);
	node_t** array = (node_t**)malloc(sizeof(node_t*)*elements);

	int i = 0;
	in_order(root, array, &i);

	int comparator(const void* node_a, const void* node_b)
	{
		node_t* node_a_ = *(node_t**)node_a;
		node_t* node_b_ = *(node_t**)node_b;

		if(node_a_->addr > node_b_->addr) return 1;
		else if(node_a_->addr == node_b_->addr) return 0;
		else return -1;
	}

	qsort(array, elements, sizeof(node_t*), comparator);

	return array;
}