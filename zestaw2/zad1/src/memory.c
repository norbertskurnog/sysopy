#include <stdlib.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//w przypadku recznej kompilacji
#ifndef BLOCK_SIZE
#define BLOCK_SIZE 2 //domyslny rozmiar bloku to 2 bajty
#endif

#include "memory.h"

node_t* bst_root;
list_t* list_begin;

void* init(size_t size)
{
	//alokacja bufora
	void* buffer = malloc(size*BLOCK_SIZE);

	//inicjalizacja drzewa i listy
	bst_root = bst_insert(NULL, buffer, size);
	list_begin = NULL;

	return buffer;
}

void* alloc(size_t size_byte)
{
	size_t blocks_alloc;

	//rozmiar obszaru podany w blokach (zaokraglenie w gore)
	if(size_byte%BLOCK_SIZE != 0) blocks_alloc = size_byte/BLOCK_SIZE + 1;
	else blocks_alloc = size_byte/BLOCK_SIZE;

	#ifdef _ALLOC_LONG_
	//przydzial pamieci z najwiekszego, wolnego fragmentu bufora
		node_t* block = bst_search_max(bst_root);
	#else
	//przydzial pamieci z najmniejszego fragmentu bufora, mogacego zmiescic size_byte bajtow
		node_t* block = bst_search_min(bst_root, blocks_alloc);
	#endif
	
	if(block == NULL) return NULL;

	void* block_beg = block->addr;
	size_t block_size = block->size;

	if(block->size < blocks_alloc){
		bst_root = defragment();

		#ifdef WITH_ALLOCATOR
			node_t* block = bst_search_max(bst_root);
		#else
			node_t* block = bst_search_min(bst_root, blocks_alloc);
		#endif

		block_size = block->size;

		if(block_size < blocks_alloc) return NULL;
	}

	void* block_end = block_beg + blocks_alloc*BLOCK_SIZE;
	
	//nowy obszar, dodawanie do listy zajetych obszarow
	list_begin = list_append(list_begin, block_beg, blocks_alloc);
	bst_root = bst_delete(bst_root, block); //usuwanie nieaktualnego wpisu w drzewie bst

	//aktualizacja drzewa bst
	if(block_size > blocks_alloc) //czy nie zaalokowano calej, dostepnej przestrzeni
		bst_root = bst_insert(bst_root, block_end, block_size-blocks_alloc);

	return block_beg;
}

node_t* defragment(node_t* root)
{
	if(root == NULL) return NULL;

	//posortowana tablica wskaznikow na elementy drzewa bst
	node_t** array = bst_sorted_array(root);
	size_t array_size = bst_elements_count(root);

	node_t* new_root = NULL;

	//"globalne" zmienne dla petli
	int cont = 0; //flaga, ustawiana dla kazdego ciaglego obszaru
	void* addr_beg = NULL;
	size_t size_sum = 0;

	int i;
	for(i=0; i<array_size; i++){
		//specjalne przypadki dla pojedynczego bloku pamieci
		if(i == array_size-1 && !cont){
			new_root = bst_insert(new_root, array[i]->addr, array[i]->size);
			return new_root;
		}
		else if(i == array_size-1 && cont){
			size_sum+=array[i]->size;
			new_root = bst_insert(new_root, addr_beg, size_sum);
			return new_root;
		}
		//aktualnie przetwarzany blok konczy sie w miejscu rozpoczecia kolejnego
		if((array[i]->addr+(array[i]->size)*BLOCK_SIZE) == array[i+1]->addr){
			if(!cont){
				addr_beg = array[i]->addr;
				cont = 1;
			}

			size_sum += array[i] -> size;
		}
		else{

			cont = 0;
			size_sum = 0;

			if(cont) new_root = bst_insert(new_root, addr_beg, size_sum);
			else new_root = bst_insert(new_root, array[i]->addr, array[i]->size);
		}
	}

	//usuwanie starego drzewa
	for(i=0; i<array_size; i++) bst_delete(bst_root, array[i]);

	return new_root;
}

memory_info_t* meminfo()
{	
	//informacje o pamieci
	memory_info_t* meminfo = malloc(sizeof(memory_info_t));
	meminfo->allocated_areas = list_elements_count(list_begin);
	meminfo->free_areas = bst_elements_count(bst_root);
	if(bst_root == NULL){
		meminfo->smallest_area = 0;
		meminfo->biggest_area = 0;
	}
	else{
		meminfo->smallest_area = (bst_search_min(bst_root, 0))->size;
		meminfo->biggest_area = (bst_search_max(bst_root))->size;
	}

	return meminfo;
}

int dealloc(void* ptr)
{
	if(ptr == NULL) return -1;

	//znajdowanie informacji o zwalnianiym obszarze pamieci na liscie
	list_t* area = list_search(list_begin, ptr);

	if(area == NULL){
		return 1;
	}

	bst_insert(bst_root, area->addr, area->size);
	list_begin = list_remove(list_begin, area);
	return 0;
}