#ifndef LIST_H
#define LIST_H

#include <stddef.h>

typedef struct list_t
{
	void* addr;
	size_t size;

	struct list_t* next;
}list_t;

list_t* list_append(list_t* first, void* addr, size_t size);
list_t* list_remove(list_t* first, list_t* element);
list_t* list_search(list_t* first, void* addr);
size_t list_elements_count(list_t* first);

#endif