#include <stdlib.h>
#include <stdio.h>

#include "list.h"

list_t* list_append(list_t* first, void* addr, size_t size)
{
	list_t* element = malloc(sizeof(list_t));
	element->addr = addr;
	element->size = size;
	element->next = NULL;

	if(first == NULL) return element;

	list_t* current_element = first;

	while(current_element->next != NULL) current_element=current_element->next;
	current_element->next = element;

	return first;
}

list_t* list_remove(list_t* first, list_t* element)
{
	if(first == NULL) return NULL;

	if(element==first){
		if(element->next != NULL){
			list_t* el = element->next;

			free(element);
			return el;
		}
		else{
			free(element);
			return NULL;
		}
	}

	list_t* current_element = first;
	list_t* prev_element = NULL;
	while(current_element != element && current_element != NULL){
		prev_element = current_element;
		current_element = current_element->next;
	}

	//blad, nie znaleziono elementu, zwraca pierwszy element lisy
	if(current_element == NULL) return first;

	prev_element->next = current_element->next;
	free(element);

	return first;
}

list_t* list_search(list_t* first, void* addr)
{
	if(first == NULL) return NULL;

	list_t* current_element = first;
	while(current_element->addr != addr){
		if(current_element == NULL) return NULL;
		current_element=current_element->next;
	}

	return current_element;
}

size_t list_elements_count(list_t* first)
{
	size_t elements = 0;

	list_t* current_element = first;
	if(first == NULL) return 0;

	while(current_element != NULL){
		elements++;
		current_element=current_element->next;	
	} 

	return elements;
}