#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

#include "bst.h"
#include "list.h"

typedef struct memory_info_t{
	size_t allocated_areas;
	size_t free_areas;
	size_t smallest_area;
	size_t biggest_area;
}memory_info_t;

void* init(size_t size);
void* alloc(size_t size);
int dealloc(void* ptr);
node_t* defragment();
memory_info_t* meminfo();

#endif