#ifndef BST_H
#define BST_H

#include <stddef.h>

typedef struct node_t
{
	void* addr;
	size_t size;

	struct node_t* parent;
	struct node_t* left_child;
	struct node_t* right_child;
}node_t;

node_t* bst_insert(node_t* root, void* addr, size_t size);

node_t* bst_search_min(node_t* root, size_t size);
node_t* bst_search_max(node_t* root);

node_t* bst_delete(node_t* root, node_t* node);
node_t* bst_succesor(node_t* root, node_t* node);

size_t bst_elements_count(node_t* root);

node_t* in_order(node_t* node, node_t** array, int* i);
node_t** bst_sorted_array(node_t* root);

#endif