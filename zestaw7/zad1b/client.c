#include "chat.h"

MSG msg;
char* client_name;
mqd_t server_queue, client_queue;

pid_t child_pid = 0;

void cleanup();
void e_wname_handler(int param);
void e_lfull_handler(int param);
void succes_handler(int param);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);
	signal(SIGRTMIN, succes_handler);
	signal(SIGRTMIN+1, e_wname_handler);
	signal(SIGRTMIN+2, e_lfull_handler);

	if(argc < 2){
		printf("Usage %s name\n", argv[0]);
		exit(1);
	}

	client_name = argv[1];
	
	/* open server queue */	
	server_queue = mq_open(COMMON_QUEUE_NAME, O_WRONLY);
	if(server_queue == -1){
		perror("IPC error: queue");
		exit(errno);
	}

	/* send id message */
	strcpy(msg.cname, client_name);
	msg.mtype = MSG_CLIENT_CONNECTED;
	msg.mtime = time(NULL);
	pid_t pid = getpid();
	char pid_str[10];
	sprintf(pid_str, "%d", pid);
	strcpy(msg.mtext, pid_str);
	mq_send(server_queue, (char*)&msg, sizeof(MSG), 0);

	printf("Trying to connect as %s...\n", msg.cname);
	pause();

	pid = fork();
	if(pid == 0){
		/* in child - refreshing chat state*/
		/* open queue created by server */
		char cqueue_name[NICK_LEN+sizeof("/_queue")];
		sprintf(cqueue_name, "/%s_queue", client_name);
		client_queue = mq_open(cqueue_name, O_RDONLY);

		while(1){
			int status = mq_receive(client_queue, (char*)&msg, sizeof(MSG), 0);

			if(status == -1){
				printf("Cannot open queue. Server not running\n");
				exit(errno);
			}
			else{
				char msg_time[32];

				if(msg.mtype == MSG_REGULAR){
					time_to_str(&msg.mtime, msg_time);
					printf("%s %s: %s\n", msg_time, msg.cname, msg.mtext);
				}
				else if(msg.mtype == MSG_CONNECTION_ACK){
					printf("Connected as %s\n", client_name);
				}
				else if(msg.mtype == MSG_SERVER_DISCONNECTED){
					printf("Server disconnected. Exit\n");
					kill(getppid(), SIGINT);
					raise(SIGINT);
				}
				else{
					printf("Error: wrong message identifier %li!\n", msg.mtype);
				}
			}
		}
	}
	else if (pid > 0){
		/* in parent - reading stdin and sending messages to server*/
		child_pid = pid;
		char buffer[MSG_LEN];
		
		while(1){
			scanf("%s", buffer);

			msg.mtype = MSG_REGULAR;
			msg.mtime = time(NULL);
			strcpy(msg.cname, client_name);
			strcpy(msg.mtext, buffer);
			mq_send(server_queue, (char*)&msg, sizeof(MSG), 0);
		}
	}
	else{
		perror("Cannot fork!\n");
		exit(errno);
	}

	return 0;
}

void cleanup()
{
	/* send disconnect message */
	strcpy(msg.cname, client_name);
	msg.mtype = MSG_CLIENT_DISCONNECTED;
	msg.mtime = time(NULL);
	memset(msg.mtext, 0, MSG_LEN);
	
	mq_send(server_queue, (char*)&msg, sizeof(MSG), 0);

	if(child_pid) kill(child_pid, SIGINT);

	mq_close(server_queue);
	mq_close(client_queue);
	exit(0);
}

void e_wname_handler(int param){
	printf("Connection rejected. Name already exists\n");
	_exit(2);
}

void e_lfull_handler(int param){
	printf("Connection rejected. Too many clients\n");
	_exit(3);
}

void succes_handler(int param){
	printf("Name ok\n");
}