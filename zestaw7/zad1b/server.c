#include "chat.h"

#define MAX_CLIENTS 128

mqd_t msg_queue;
cqueue_t* clients[MAX_CLIENTS];
int clients_connected = 0;

void cleanup();

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	int debug = 0;

	if(argc > 1 && !strcmp(argv[1],"-d")) debug = 1;

	MSG msg, msg_ack;

	struct mq_attr attr;
	attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(MSG);
    attr.mq_curmsgs = 0;

	msg_queue = mq_open(COMMON_QUEUE_NAME, O_CREAT, S_IRWXU, &attr);
	if(msg_queue == -1){
		perror("IPC error queue");
		exit(errno);
	}

	char msg_time[32];

 	/* main loop */
 	int bytes_read;
	while(1){
		
		bytes_read = mq_receive(msg_queue, (char*)&msg, sizeof(MSG), 0);

		if(bytes_read <= 0){
			printf("Error receiving message\n");
		}
		else{
			if(debug) time_to_str(&msg.mtime, msg_time);

			int nick_exist = 0;
			if(msg.mtype == MSG_CLIENT_CONNECTED){

				/* get client pid */
				int pid;
				sscanf(msg.mtext, "%d", &pid);

				/* check if not too many clients */
				if(MAX_CLIENTS == clients_connected) sigqueue(pid, SIGRTMIN+2, (union sigval)0);

				/* check if client with this name is already connected */
				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] && !strcmp(clients[i]->cname, msg.cname)){
						printf("Client with \"%s\" already connected\n", msg.mtext);

						/* send error signal to client */
						sigqueue(pid, SIGRTMIN+1, (union sigval)0);
						nick_exist = 1;

						break;
					}
				}

				if(nick_exist) continue;

				if(!debug) time_to_str(&msg.mtime, msg_time);
				printf("%s: %s connected!\n", msg_time, msg.cname);

				/* new queue for each client */
				char cqueue_name[NICK_LEN+sizeof("/_queue")];
				sprintf(cqueue_name, "/%s_queue", msg.cname);

				attr.mq_flags = O_NONBLOCK;
				cqueue_t* client_queue = (cqueue_t*) malloc(sizeof(client_queue));
				client_queue->q_desc = mq_open(cqueue_name, O_CREAT|O_WRONLY, S_IRWXU, &attr);
				strcpy(client_queue->cname, msg.cname);
				printf("Name added=%s\n", client_queue->cname);

				/* find first free index in array */
				int free_index = clients_connected;

				for(i=0; i<clients_connected; i++){
					if(!clients[i]){
						free_index = i;
						break;
					}
				}

				/* save new client data */
				clients[free_index] = client_queue;

				if(clients[free_index]->q_desc == -1){
					perror("IPC error queue");
					exit(errno);
				}

				/* send ack */
				msg_ack.mtype = MSG_CONNECTION_ACK;
				msg_ack.mtime = time(NULL);
				mq_send(clients[free_index]->q_desc, (char*)&msg_ack, sizeof(MSG), 0);
				
				/* add new client if no free indices */
				if(free_index == clients_connected) clients_connected++;

				/* send succes signal */
				sigqueue(pid, SIGRTMIN, (union sigval)0);
			}
			else if(msg.mtype == MSG_REGULAR){
				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] != NULL){
						mq_send(clients[i]->q_desc, (char*)&msg, sizeof(MSG), 0);
					}
				}

				if(debug) printf("Received message from %s\n", msg.cname);
			}
			else if(msg.mtype == MSG_CLIENT_DISCONNECTED){
				printf("%s: %s disconnected!\n", msg_time, msg.cname);

				char cqueue_name[NICK_LEN+sizeof("/_queue")];
				sprintf(cqueue_name, "/%s_queue", msg.cname);
				mq_unlink(cqueue_name);

				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] && !strcmp(clients[i]->cname, msg.cname)){
						clients[i]->q_desc = -1;
						clients[i] = NULL;
						break;
					}
				}
			}
			else{
			 	printf("Error: wrong message identifier %li!\n", msg.mtype);
			}
		}
	}

	exit(0);
}

void cleanup()
{
	int i;
	for(i=0; i<clients_connected; i++){
		if(clients[i]){
			MSG msg;
			msg.mtype = MSG_SERVER_DISCONNECTED;
			mq_send(clients[i]->q_desc, (char*)&msg, sizeof(MSG), 0);
			mq_close(clients[i]->q_desc);
		}
	}
	mq_unlink(COMMON_QUEUE_NAME);

	exit(0);
}