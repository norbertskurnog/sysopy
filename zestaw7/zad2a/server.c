#include "common.h"

#define MAX_CLIENTS 128

int msg_queue;

void cleanup();
void time_to_str(time_t* raw_time, char* buffer);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	if(argc < 2){
		printf("Usage: %s max_file_size\n", argv[0]);
		exit(1);
	}

	int no_space = 0;
	size_t curr_fsize = 0, max_fsize;
	sscanf(argv[1], "%li", &max_fsize);
	if(max_fsize <= 0){
		printf("File size must be positive integer!\n");
		exit(2);
	}

	char filename[] = "out_file";
	FILE* out_file = fopen(filename, "w+");
	if(!out_file){
		printf("Cannot open out file\n");
		exit(1);
	}

	MSG msg;

	/* open queue */
    key_t mq_key = ftok("/tmp", 123);
    msg_queue = msgget(mq_key, IPC_CREAT | S_IRWXU);

	if(msg_queue == -1){
		perror("IPC error queue");
		exit(errno);
	}
 	/* main loop */
 	int bytes_read;
	while(1){
		bytes_read = msgrcv(msg_queue, &msg, sizeof(MSG), 0, 0);
		if(no_space){
			msg.mtype = MSG_NO_SPACE;
			msgsnd(msg_queue, &msg, sizeof(MSG), 0);
			continue;
		}

		if(bytes_read <= 0){
			printf("Error receiving message\n");
		}
		else{
			if(msg.mtype == MSG_REGULAR){

				printf("Got message\n");
				char buff[2*sizeof(MSG)];

				time_t raw_time = time(NULL);
				char time_buff[35];
				time_to_str(&raw_time, time_buff);

				sprintf(buff, "%s\n%s\n%s\n", time_buff, msg.cname, msg.mtext);
				int space_to_take = strlen(buff);

				if(curr_fsize + space_to_take > max_fsize){
					no_space = 1;
					msg.mtype = MSG_NO_SPACE;
					msgsnd(msg_queue, &msg, sizeof(MSG), 0);
					continue;
				}

				if(fprintf(out_file, "%s", buff) < 0){
					perror("File error: ");
				}
				fflush(out_file);

				curr_fsize += space_to_take;

				/* send ack */
				msg.mtype = MSG_OK;
				msgsnd(msg_queue, &msg, sizeof(MSG), 0);
			}
			else{
			 	printf("Error: wrong message identifier %li!\n", msg.mtype);
			}
		}
	}

	exit(0);
}

void cleanup()
{
	//msgctl(msg_queue, IPC_RMID, NULL);

	exit(0);
}

void time_to_str(time_t* raw_time, char* buffer){

	struct tm* tm_info;
	tm_info = localtime(raw_time);
	strftime(buffer, 32, "%d-%m-%y %H:%M:%S", tm_info);
}