#include "common.h"

MSG msg;
char* client_name;
int server_queue;

#define RND_MSG_SIZE 25

void cleanup();

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	if(argc < 2){
		printf("Usage %s name\n", argv[0]);
		exit(1);
	}

	client_name = argv[1];
	
	/* open server queue */	
    key_t mq_key = ftok("/tmp", 123);
	server_queue = msgget(mq_key, IPC_CREAT | S_IRWXU);


	/* send message */
	strcpy(msg.cname, client_name);
	msg.mtype = MSG_REGULAR;
	msg.cpid = getpid();

	int i;
	for(i=0; i<RND_MSG_SIZE; i++){
		msg.mtext[i] = 'A' + (rand() % 26);
	}
	
	int snd = msgsnd(server_queue, &msg, sizeof(MSG), 0);
	if(snd < 0){
		perror("Cannot send message\n");
	}

	int status = msgrcv(server_queue, &msg, sizeof(MSG), 0, 0);

	if(status == -1){
		printf("Cannot open queue. Server not running\n");
		exit(errno);
	}
	else{
		if(msg.mtype == MSG_OK)
			printf("Data successfully written to file\n");
		else if(msg.mtype == MSG_NO_SPACE)
			printf("File is full... Cannot write.\n");
		else
			printf("Wrong message identifier %li", msg.mtype);
	}

	return 0;
}

void cleanup()
{
	msgctl(server_queue, IPC_RMID, NULL);
	exit(0);
}