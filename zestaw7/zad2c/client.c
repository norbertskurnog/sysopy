#include "common.h"

MSG msg;
char* client_name;
mqd_t server_queue;

#define RND_MSG_SIZE 25

void cleanup();
void msg_receive(int param);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);
	signal(SIGUSR1, msg_receive);

	if(argc < 2){
		printf("Usage %s name\n", argv[0]);
		exit(1);
	}

	client_name = argv[1];
	
	/* open server queue */	
	server_queue = mq_open(COMMON_QUEUE_NAME, O_RDWR);
	if(server_queue == -1){
		perror("IPC error: queue");
		exit(errno);
	}

	/* send message */
	strcpy(msg.cname, client_name);
	msg.mtype = MSG_REGULAR;
	msg.cpid = getpid();

	/* generate random string */
	int i;
	for(i=0; i<RND_MSG_SIZE; i++){
		msg.mtext[i] = 'A' + (rand() % 26);
	}
	
	int snd = mq_send(server_queue, (char*)&msg, sizeof(MSG), 0);
	if(snd < 0){
		perror("Cannot send message\n");
	}

	/* wait for notification */
	struct sigevent sev;
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGUSR1;
	mq_notify(server_queue, &sev);

	while(1);

	return 0;
}

void cleanup()
{
	mq_close(server_queue);
	exit(0);
}

void msg_receive(int param)
{
	int status = mq_receive(server_queue, (char*)&msg, sizeof(MSG), 0);

	if(status == -1){
		printf("Cannot open queue. Server not running\n");
		exit(errno);
	}
	else{
		if(msg.mtype == MSG_OK)
			printf("Data successfully wrote to file\n");
		else if(msg.mtype == MSG_NO_SPACE)
			printf("File is full... Cannot write.\n");
		else
			printf("Wrong message identifier %li", msg.mtype);
	}

	exit(0);
}