#include "common.h"

#define MAX_CLIENTS 128

MSG msg;
mqd_t msg_queue;
struct sigevent sev;
int no_space = 0;
size_t curr_fsize = 0, max_fsize;

FILE* out_file;
char filename[] = "out_file";

void cleanup();
void time_to_str(time_t* raw_time, char* buffer);
void msg_receive(int param);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);
	signal(SIGUSR1, msg_receive);

	if(argc < 2){
		printf("Usage: %s max_file_size\n", argv[0]);
		exit(1);
	}

	sscanf(argv[1], "%li", &max_fsize);
	if(max_fsize <= 0){
		printf("File size must be positive integer!\n");
		exit(2);
	}

	out_file = fopen(filename, "w+");
	if(!out_file){
		printf("Cannot open out file\n");
		exit(1);
	}


	struct mq_attr attr;
	attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(MSG);
    attr.mq_curmsgs = 0;

	msg_queue = mq_open(COMMON_QUEUE_NAME, O_CREAT | O_RDWR, S_IRWXU, &attr);
	if(msg_queue == -1){
		perror("IPC error queue");
		exit(errno);
	}

	/* wait for notification */

	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGUSR1;
	mq_notify(msg_queue, &sev);

 	/* main loop */
	while(1);

	exit(0);
}

void cleanup()
{
	mq_unlink(COMMON_QUEUE_NAME);

	exit(0);
}

void time_to_str(time_t* raw_time, char* buffer){

	struct tm* tm_info;
	tm_info = localtime(raw_time);
	strftime(buffer, 32, "%d-%m-%y %H:%M:%S", tm_info);
}

void msg_receive(int param)
{
 	int bytes_read = mq_receive(msg_queue, (char*)&msg, sizeof(MSG), 0);
	if(no_space){
		msg.mtype = MSG_NO_SPACE;
		mq_send(msg_queue, (char*)&msg, sizeof(MSG), 0);
		mq_notify(msg_queue, &sev);
		return;
	}

	if(bytes_read <= 0){
		printf("Error receiving message\n");
	}
	else{
		if(msg.mtype == MSG_REGULAR){
			char buff[2*sizeof(MSG)];

			time_t raw_time = time(NULL);
			char time_buff[35];
			time_to_str(&raw_time, time_buff);

			sprintf(buff, "%s\n%s\n%s\n", time_buff, msg.cname, msg.mtext);
			int space_to_take = strlen(buff);

			if(curr_fsize + space_to_take > max_fsize){
				no_space = 1;
				msg.mtype = MSG_NO_SPACE;
				mq_send(msg_queue, (char*)&msg, sizeof(MSG), 0);
				mq_notify(msg_queue, &sev);
				return;
			}

			if(fprintf(out_file, "%s", buff) < 0){
				perror("File error: ");
			}
			fflush(out_file);

			curr_fsize += space_to_take;

			/* send ack */
			msg.mtype = MSG_OK;
			mq_send(msg_queue, (char*)&msg, sizeof(MSG), 0);

			printf("Data wrote to %s\n", filename);

			mq_notify(msg_queue, &sev);

			sleep(1);
		}
		else{
		 	printf("Error: wrong message identifier %li!\n", msg.mtype);
		}
	}
	mq_notify(msg_queue, &sev);
}