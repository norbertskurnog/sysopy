#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <time.h>

#define COMMON_QUEUE_NAME "/chat_queue"

#define MSG_LEN 512
#define NICK_LEN 32

#define MSG_REGULAR 2
#define MSG_CLIENT_CONNECTED 1
#define MSG_CLIENT_DISCONNECTED 3
#define MSG_CONNECTION_ACK 4
#define MSG_SERVER_DISCONNECTED 5

typedef struct MSG{
	long mtype;
	time_t mtime;
	char cname[NICK_LEN];
	char mtext[MSG_LEN];
}MSG;

typedef struct CQUEUE{
	int q_desc;
	char cname[25];
}cqueue_t;

void time_to_str(time_t* raw_time, char* buffer){

	struct tm* tm_info;
	tm_info = localtime(raw_time);
	strftime(buffer, 32, "%d-%m-%y %H:%M:%S", tm_info);
}