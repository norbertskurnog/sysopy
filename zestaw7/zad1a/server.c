#include "chat.h"

#define MAX_CLIENTS 128

int msg_queue;
cqueue_t* clients[MAX_CLIENTS];
int clients_connected = 0;
int queues_opened = 128;

void cleanup(int param);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	// int debug = 0;

	// if(argc > 1 && !strcmp(argv[1],"-d")) debug = 1;

	key_t msgq_key = ftok("/bin/sh", 123);
	if(msgq_key == (key_t)-1){
		perror("IPC error: ftok");
		exit(errno);
	}

	msg_queue = msgget(msgq_key, IPC_CREAT | S_IRWXU);
	if(msg_queue == -1){
		perror("IPC error queue");
		exit(errno);
	}

	MSG msg, msg_ack;
	char msg_time[32];

	struct msqid_ds info;
	msgctl(msg_queue, IPC_STAT, &info);
 	
 	/* main loop */
 	int bytes_read;
	while(1){
		bytes_read = msgrcv(msg_queue, &msg, sizeof(MSG), 0, 0);
		
		if(bytes_read <= 0){
			printf("Error receiving message\n");
		}
		else{
			time_to_str(&msg.mtime, msg_time);

			int nick_exist = 0;
			if(msg.mtype == MSG_CLIENT_CONNECTED){
				
				/* get client pid */
				int pid;
				sscanf(msg.mtext, "%d", &pid);

				/* check if not too many clients */
				if(MAX_CLIENTS == clients_connected) sigqueue(pid, SIGRTMIN+2, (union sigval)0);

				/* check if client with this name is already connected */
				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] && !strcmp(clients[i]->cname, msg.cname)){
						printf("Client with \"%s\" already connected\n", msg.mtext);

						/* send error signal to client */
						sigqueue(pid, SIGRTMIN+1, (union sigval)0);
						nick_exist = 1;

						break;
					}
				}

				if(nick_exist) continue;

				time_to_str(&msg.mtime, msg_time);
				printf("%s: %s connected!\n", msg_time, msg.cname);

				cqueue_t* client_queue = (cqueue_t*) malloc(sizeof(client_queue));

				/* opening message queue */
				key_t cq_key = ftok("/tmp", queues_opened);
				if(cq_key == (key_t)-1){
					perror("IPC error: ftok");
					exit(errno);
				}

				int c_queue = msgget(cq_key, IPC_CREAT | S_IRWXU);
				if(c_queue == -1){
					perror("IPC error: queue");
					exit(errno);
				}

				client_queue->q_desc = c_queue;
				strcpy(client_queue->cname, msg.cname);
				printf("Name added=%s\n", client_queue->cname);

				/* find first free index in array */
				int free_index = clients_connected;

				for(i=0; i<clients_connected; i++){
					if(!clients[i]){
						free_index = i;
						break;
					}
				}

				/* save new client data */
				clients[free_index] = client_queue;

				if(clients[free_index]->q_desc == -1){
					perror("IPC error queue");
					exit(errno);
				}

				/* send ack */
				msg_ack.mtype = MSG_CONNECTION_ACK;
				msg_ack.mtime = time(NULL);
				msgsnd(clients[free_index]->q_desc, &msg_ack, sizeof(MSG), 0);
				
				/* add new client if no free indices */
				if(free_index == clients_connected) clients_connected++;

				/* send succes signal */
				/* send queue id key to client */
				union sigval param;
				param.sival_int = queues_opened;
				sigqueue(pid, SIGRTMIN, param);
				queues_opened++;
			}
			else if(msg.mtype == MSG_REGULAR){
				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] != NULL){
						msgsnd(clients[i]->q_desc, &msg, sizeof(MSG), 0);
					}
				}
			}
			else if(msg.mtype == MSG_CLIENT_DISCONNECTED){
				printf("%s: %s disconnected!\n", msg_time, msg.cname);

				int i;
				for(i=0; i<clients_connected; i++){
					if(clients[i] && !strcmp(clients[i]->cname, msg.cname)){
						msgctl(clients[i]->q_desc, IPC_RMID, NULL);
						clients[i]->q_desc = -1;
						clients[i] = NULL;
						break;
					}
				}
			}
			else{
				printf("Error: wrong message identifier %li\n!", msg.mtype);
			}
		}
	}

	exit(0);
}

void cleanup(int param)
{
	msgctl(msg_queue, IPC_RMID, NULL);
	int i;
	for(i=0; i<clients_connected; i++){
		if(clients[i]){
			MSG msg;
			msg.mtype = MSG_SERVER_DISCONNECTED;
			msgsnd(clients[i]->q_desc, &msg, sizeof(MSG), 0);
			msgctl(clients[i]->q_desc, IPC_RMID, NULL);
		}
	}
	msgctl(msg_queue, IPC_RMID, NULL);

	exit(0);
}