#include "chat.h"

MSG msg;
char* client_name;
int server_queue, client_queue;
int cq_key;

pid_t child_pid = 0;

void cleanup(int param);
void e_wname_handler(int param);
void e_lfull_handler(int param);
void success_handler(int param, siginfo_t* sig_info, void* params);

int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);
	//signal(SIGRTMIN, succes_handler);
	struct sigaction success_action;
	memset(&success_action, 0, sizeof(success_action));
	success_action.sa_sigaction = success_handler;
	sigemptyset(&success_action.sa_mask);
	success_action.sa_flags = SA_SIGINFO;
	sigaction(SIGRTMIN, &success_action, NULL);

	signal(SIGRTMIN+1, e_wname_handler);
	signal(SIGRTMIN+2, e_lfull_handler);

	if(argc < 2){
		printf("Usage %s name\n", argv[0]);
		exit(1);
	}

	client_name = argv[1];

	/* opening message queue */
	key_t msgq_key = ftok("/bin/sh", 123);
	if(msgq_key == (key_t)-1){
		perror("IPC error: ftok");
		exit(errno);
	}

	server_queue = msgget(msgq_key, S_IRWXU);
	if(server_queue == -1){
		perror("IPC error: queue");
		exit(errno);
	}

	/* send id message */
	strcpy(msg.cname, argv[1]);
	msg.mtype = MSG_CLIENT_CONNECTED;
	msg.mtime = time(NULL);
	pid_t pid = getpid();
	char pid_str[10];
	sprintf(pid_str, "%d", pid);
	strcpy(msg.mtext, pid_str);
	msgsnd(server_queue, &msg, sizeof(MSG), 0);

	printf("Trying to connect as %s...\n", msg.cname);
	pause();

	pid = fork();
	if(pid == 0){
		/* in child - refreshing chat state*/
		/* open queue created by server */
		key_t key = ftok("/tmp", cq_key);
		if(key == (key_t)-1){
			perror("IPC error: ftok");
			exit(errno);
		}

		int client_queue = msgget(key, S_IRWXU);
		if(client_queue == -1){
			perror("IPC error: queue");
			exit(errno);
		}

		while(1){
			int status = msgrcv(client_queue, &msg, sizeof(MSG), 0, 0);

			if(status == -1){
				printf("Cannot open queue. Server not running\n");
				exit(errno);
			}
			else{
				char msg_time[32];

				if(msg.mtype == MSG_REGULAR){
					time_to_str(&msg.mtime, msg_time);
					printf("%s %s: %s\n", msg_time, msg.cname, msg.mtext);
				}
				else if(msg.mtype == MSG_CONNECTION_ACK){
					printf("Connected as %s\n", client_name);
				}
				else if(msg.mtype == MSG_SERVER_DISCONNECTED){
					printf("Server disconnected. Exit\n");
					kill(getppid(), SIGINT);
					raise(SIGINT);
				}
				else{
					printf("Error: wrong message identifier %li!\n", msg.mtype);
				}
			}
		}
	}
	else if (pid > 0){
		/* in parent - reading stdin and sending messages to server*/
		child_pid = pid;
		char buffer[MSG_LEN];
		
		while(1){
			scanf("%s", buffer);

			msg.mtype = MSG_REGULAR;
			msg.mtime = time(NULL);
			strcpy(msg.cname, client_name);
			strcpy(msg.mtext, buffer);
			msgsnd(server_queue, &msg, sizeof(MSG), 0);
		}
	}
	else{
		perror("Cannot fork!\n");
		exit(errno);
	}

	return 0;
}

void cleanup(int param)
{
	/* send disconnect message */
	strcpy(msg.cname, client_name);
	msg.mtype = MSG_CLIENT_DISCONNECTED;
	msg.mtime = time(NULL);
	memset(msg.mtext, 0, MSG_LEN);
	
	msgsnd(server_queue, &msg, sizeof(MSG), 0);

	if(child_pid) kill(child_pid, SIGINT);

	exit(0);
}

void e_wname_handler(int param){
	printf("Connection rejected. Name already exists\n");
	_exit(2);
}

void e_lfull_handler(int param){
	printf("Connection rejected. Too many clients\n");
	_exit(3);
}

void success_handler(int param, siginfo_t* sig_info, void* params){
	printf("Name ok\n");
	cq_key = sig_info->si_value.sival_int;
}