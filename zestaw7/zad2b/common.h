#include <sys/types.h>
#include <mqueue.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <time.h>

#define COMMON_QUEUE_NAME "/chat_queue"

#define MSG_LEN 512
#define NICK_LEN 32

#define MSG_REGULAR 2
#define MSG_OK 1
#define MSG_NO_SPACE 3

typedef struct MSG{
	long mtype;
	char cname[NICK_LEN];
	pid_t cpid;
	char mtext[MSG_LEN];
}MSG;