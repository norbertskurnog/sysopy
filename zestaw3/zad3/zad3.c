#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "macros.h"

int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len);
pid_t lock_test(int fd, int type, off_t offset, int whence, int len);
void test_bytes(int fd);
short lock_type(int fd, int byte);

void commands()
{
	printf("help\t\t\tWyswietla pomoc\n");
	printf("quit\t\t\tWychodzi z programu\n");
	printf("set read [n]\t\tUstawia rygiel odczytu na n-ty znak\n");
	printf("set write [n]\t\tUstawia rygiel zapisu na n-ty znak\n");
	printf("list\t\tLista zalozonych rygli\n");
	printf("unlock [n]\t\tzwolnenie rygla na n-ty znak\n");
	printf("read [n]\t\tOdczyt n-tego znaku z pliku\n");
	printf("write [n]\t\tZapis na n-ty znak\n");
}

int main(int argc, char** argv)
{
	if(argc<2)
	{
		printf("usage: %s file\n", argv[0]);
		exit(1);
	}

	int fd = open(argv[1], O_RDWR);
	int error_code = errno;
	if(fd == -1)
	{
		fprintf(stderr, "Cannot open file %s\n", argv[1]);
		exit(error_code);
	}

	printf("\nWpisz \"help\" aby uzyskac liste dostepnych opcji\n");

	//main loop
	while(1)
	{
		printf(">> ");
		char buf[256];
	
		fgets(buf, 256, stdin);
		
		//parsing command
		char* cmd[3] = {NULL, NULL, NULL};
		cmd[0] = strtok(buf, " \t\n");

		int i = 0;
		while(cmd[i] && i<2)
			cmd[++i] = strtok(NULL, " \t\n");

		if(strcmp(cmd[0], "quit") == 0) break;
		else if(strcmp(cmd[0], "help") == 0) commands();
		else if(strcmp(cmd[0], "set") == 0)
		{
			if(!cmd[1] || !cmd[2])
				printf("Blad skladni. Napisz \"help\" aby zobaczyc mozliwe polecenia\n");
			else if(strcmp(cmd[1], "read") == 0)
			{
				int offset = atoi(cmd[2]);
				int pid =  is_read_lockable(fd, offset, SEEK_SET, 1);
				if(pid == 0)
				{
					// blokuje
					if(read_lock(fd, offset, SEEK_SET, 1) < 0)
						printf("Nie mozna zablokowac\n");
					else
						printf("%d bajt zablokowany\n", offset);
				}
				else printf("Nie da sie zablokowac: PID=%d\n", pid);
			}
			if(strcmp(cmd[1], "write") == 0)
			{
				int offset = atoi(cmd[2]);
				int pid =is_write_lockable(fd, offset, SEEK_SET, 1);
				if(pid == 0)
				{
					if(write_lock(fd, offset, SEEK_SET, 1) < 0)
						printf("Nie mozna zablokowac\n");
					else
						printf("%d bajt zablokowany\n", offset);
				}
				else printf("Nie da sie zablokowac: PID=%d\n", pid);
			}
		}
		else if(strcmp(cmd[0], "unlock") == 0)
		{
			int offset = atoi(cmd[1]);
			if(un_lock(fd, offset, SEEK_SET, 1) < 0)
				printf("Nie mozna odblokowac\n");
			else
				printf("%d bajt odblokowano\n", offset);
		}
		else if(strcmp(cmd[0], "list") == 0)
			test_bytes(fd);
		else if(strcmp(cmd[0], "read") == 0)
		{
			if(!cmd[1])
				printf("Zla skladnia. Wpisz \"help\" aby wyswietlic dostepne opcje\n");
			else
			{
				char buf;
				lseek(fd, atoi(cmd[1]), SEEK_SET);
				if(lock_type(fd, atoi(cmd[1])) != F_RDLCK)
				{
					if((read(fd, &buf, 1)) < 0)
						printf("Nie moge odczytac\n");
					else
						printf("Wczytano: %c\n", buf);
				}
				else printf("Nie mozna odczytac - WRITEONLY\n");
			}
		}
		else if(strcmp(cmd[0], "write") == 0)
		{
			if(!cmd[1] || !cmd[2])
				printf("Zla skladnia. Wpisz \"help\" aby wyswietlic dostepne opcje\n");
			else
			{
				char buf = cmd[2][0];
				lseek(fd, atoi(cmd[1]), SEEK_SET);
				if(lock_type(fd, atoi(cmd[1])) != F_WRLCK)
				{
					if((write(fd, &buf, 1) < 0))
						printf("Nie moge zamienic\n");
					else
						printf("Zmieniono na: %c\n", buf);
				}
				else printf("Nie mozna zapisac - READONLY\n");
			}
		}
		else commands();
	}


	close(fd);

	return 0;
}
