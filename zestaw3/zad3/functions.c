#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include "macros.h"

int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
	struct flock lock;

	lock.l_type = type;
	lock.l_start = offset;
	lock.l_whence = whence;
	lock.l_len = len;

	return (fcntl(fd, cmd, &lock));
}

pid_t lock_test(int fd, int type, off_t offset, int whence, int len)
{
	struct flock lock;

	lock.l_type = type;
	lock.l_start = offset;
	lock.l_whence = whence;
	lock.l_len = len;
	if(fcntl(fd, F_GETLK, &lock) < 0)
	{
		int error_code = errno;
		fprintf(stderr, "Blad fcntl\n");
		exit(error_code);
	}

	if(lock.l_type == F_UNLCK)
		return 0;

	return lock.l_pid;
}

short lock_type(int fd, int byte)
{
	struct flock lock;

	lock.l_type = F_WRLCK;
	lock.l_start = byte;
	lock.l_whence = SEEK_SET;
	lock.l_len = 1;
	if(fcntl(fd, F_GETLK, &lock) < 0)
	{
		int error_code = errno;
		fprintf(stderr, "Blad fcntl\n");
		exit(error_code);
	}

	return lock.l_type;
}

void test_bytes(int fd)
{
	int i = 0;

	int end = lseek(fd, 0, SEEK_END);

	for(i = 0 ; i < end ; i++)
	{
		int type = lock_type(fd, i);
		if(type == F_WRLCK)
			printf("%d locked \t READONLY \t PID=%d\n", i, is_write_lockable(fd, i, SEEK_SET, 1));
		if(type == F_RDLCK)
			printf("%d locked \t WRITEONLY \t PID=%d\n", i, is_read_lockable(fd, i, SEEK_SET, 1));
	}
}
