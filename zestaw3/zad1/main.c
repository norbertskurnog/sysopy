#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stddef.h>

#include "functions.h"

int main(int argc, char* argv[]) 
{
	//result file
	//FILE* results_file = fopen("wynik.txt", "w");

	//parse command line
	char usage_message[] = "Usage: %s ((generuj struct_count struct_size) | sortuj) filename \n";


	if(argc < 3){
		printf(usage_message, argv[0]);
		return 1;
	}

	if (!strcmp(argv[1], "generuj") && argc >= 5){
		size_t struct_count = atoi(argv[2]);
		size_t struct_size = atoi(argv[3]);

		if(struct_size < 1 || struct_count < 1){
			printf("Struct size and number of struct must be positive integers!\n");
			return 1;
		}

		generate(argv[4], struct_size, struct_count);

	}
	else if (!strcmp(argv[1], "sortuj") && argc >= 3){
		//print_file(argv[2]);
		sort(argv[2],0);
		//print_file(argv[2]);
		
	}
	else{
		printf(usage_message, argv[0]);
		return 1;
	}

	return 0;
}