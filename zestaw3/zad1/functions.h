#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stddef.h>
#include <stdlib.h>
#include <errno.h> 
#include <stdio.h>
#include <time.h>

#ifdef SYS
#include <unistd.h>
#include <fcntl.h>
#endif

typedef struct struct_t
{
	int key;
	char* data;
}struct_t;


void pr_struct(const struct_t* struct_print, size_t struct_size);

void generate(const char* filename, size_t struct_size, size_t struct_count);
void sort(const char* filename, int verbose);
void print_file(char* filename);

#ifndef SYS
void read_struct(FILE* file, struct_t* struct_res, size_t struct_size);
#else
struct_t* read_struct(int file, size_t struct__size, size_t struct_size);
#endif

#ifndef SYS
void write_struct(FILE* file, const struct_t* struct_write, size_t struct_size);
#else
void write_struct(int file, const struct_t* struct_write, size_t struct_size);
#endif


#endif