#include "functions.h"

void pr_struct(const struct_t* struct_print, size_t struct_size)
{
	printf("Key: %d \nData: ", struct_print->key);

	int i;
	for(i=0; i<struct_size; i++){
		printf("%d ", struct_print->data[i]);
	}
	printf("\n");
}

#ifndef SYS
void read_struct(FILE* file, struct_t* struct_read, size_t struct_size)
#else
struct_t* read_struct(int file, size_t struct_size, size_t struct_size)
#endif
{
	#ifndef SYS
	fread(&struct_read->key, 1, sizeof(int), file);
	fread(struct_read->data, struct_size, sizeof(char), file);
	#else
	read(file, &struct_read->key, sizeof(int));
	read(file, struct_read->data, struct_size);
	#endif
}

/*Function does not check if index is correct!*/
#ifndef SYS
void write_struct(FILE* file, const struct_t* struct_write, size_t struct_size)
#else
void write_struct(int file, const struct_t* struct_write, size_t struct_size)
#endif
{
	#ifndef SYS
	fwrite(&struct_write->key, 1, sizeof(int), file);
	fwrite(struct_write->data, sizeof(char), struct_size, file);
	#else
	write(file, &struct_write->key, sizeof(int));
	write(file, struct_write->data, sizeof(char));
	#endif
}

void generate(const char* filename, size_t struct_size, size_t struct_count)
{
	#ifndef SYS
	FILE * file = fopen(filename, "wb+");
	#else
	int file = open(filename, O_CREAT | O_RDWR);
	#endif

	#ifndef SYS
	if(file == NULL)
	#else
	if(file == -1)
	#endif
	{
		fprintf(stderr, "Cannot open file: %s\n", filename);
		exit(errno);
	}

	srand(time(NULL));
	
	//file header
	#ifndef SYS
	fwrite(&struct_size, sizeof(size_t), 1, file);
	fwrite(&struct_count, sizeof(size_t), 1, file);
	#else
	write(file, &struct_size, sizeof(size_t));
	write(file, &struct_count, sizeof(size_t));
	#endif

	//writing actual data
	int i;
	for(i = 0; i<struct_count; i++)
	{
		//alloc next struct to write
		struct_t* fstruct = (struct_t*)malloc(sizeof(struct_t));

		fstruct->key = rand() % 100;		
		fstruct->data = (char*)malloc(sizeof(char)*struct_size);

		//generate data
		int j;
		for(j=0; j<struct_size; j++){
			fstruct->data[j] = (char)(rand() % 255);
		}

		//write to file
		#ifndef SYS
		fwrite(&fstruct->key, sizeof(int), 1, file);
		fwrite(fstruct->data, sizeof(char), struct_size, file);
		#else
		write(file, &fstruct->key, sizeof(int));
		write(file, fstruct->data, struct_size);
		#endif

	}
	#ifndef SYS
	fclose(file);
	#else
	close(file);
	#endif
}

void sort(const char* filename)
{
	size_t struct_size, struct_count;
	
	#ifndef SYS
	FILE * file = fopen(filename, "r+b");
	#else
	int file = open(filename, O_RDWR);
	#endif

	//remember error code
	int error_code = errno;

	#ifndef SYS
	if(file == NULL)
	#else
	if(file == -1)
	#endif

	{
		fprintf(stderr, "File %s not found!\n", filename);
		exit(error_code);
	}

	//read header
	#ifndef SYS
	fread(&struct_size, sizeof(size_t), 1, file);
	fread(&struct_count, sizeof(size_t), 1, file);
	#else
	read(file, &struct_size, sizeof(size_t));
	read(file, &struct_count, sizeof(size_t));
	#endif

	//allocate memory for two structs
	struct_t *struct1, *struct2;

	struct1 = (struct_t*)malloc(sizeof(struct_t));
	struct1->data = (char*)malloc(sizeof(char)*struct_size);

	struct2 = (struct_t*)malloc(sizeof(struct_t));
	struct2->data = (char*)malloc(sizeof(char)*struct_size);

	//actual sorting
	int i, j;
	for(i=0 ; i<struct_count-1; i++)
	{
		#ifndef SYS
		fseek(file, 2*sizeof(size_t), SEEK_SET);
		#else
		lseek(file, 2*sizeof(size_t), SEEK_SET);
		#endif

		//bubble sort
		for(j=0; j<struct_count-i-1; j++)
		{
			//read two structs to be compared
			fseek(file, 2*sizeof(size_t)+j*(sizeof(int)+sizeof(char)*struct_size), SEEK_SET);
			read_struct(file, struct1, struct_size);
			read_struct(file, struct2, struct_size);

			//compare and swap if nessesary
			if(struct1->key > struct2->key)
			{
				fseek(file, 2*sizeof(size_t)+j*(sizeof(int)+sizeof(char)*struct_size), SEEK_SET);
				write_struct(file, struct2, struct_size);
				write_struct(file, struct1, struct_size);
			}
		}
	}	
	
	free(struct1);
	free(struct2);


	#ifndef SYS
	fclose(file);
	#else
	close(file);
	#endif
}

void print_file(char* filename)
{
	#ifndef SYS
	FILE* file = fopen(filename, "rw");

	if(file == NULL)
	#else
	int file = open(filename, O_RDONLY);
	if(file == -1)
	#endif
	{
		printf("Cannot open file %s!\n", filename);
		exit(2);
	}

	size_t struct_size, struct_count;

	//read header
	#ifndef SYS
	fread(&struct_size, 1, sizeof(size_t), file);
	fread(&struct_count, 1, sizeof(size_t), file);
	#else
	read(file, &struct_size, sizeof(size_t));
	read(file, &struct_count, sizeof(size_t));
	#endif

	printf("Structs in file: %zu, struct size: %zu\n", struct_count, struct_size);

	//read the rest of file
	struct_t* struct_print;
	int i;
	for(i=0; i<struct_count; i++){
		read_struct(file, struct_print, struct_size);
		pr_struct(struct_print, struct_size);
	}
}

