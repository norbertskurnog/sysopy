#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "zad2a.h"

int main(int argc, char ** argv)
{
	if(argc<3)
	{
		printf("usage: %s dir rights\n", argv[0]);
		exit(1);
	}

	struct stat s;
	if(stat(argv[1], &s) == -1)
	{
		int error_code = errno;
		fprintf(stderr, "Stat error\n");
		exit(error_code);
	}

	if(!S_ISDIR(s.st_mode))
	{
		printf("%s is not a directory!\n", argv[1]);
		exit(-1);
	}

	printf("Files in directory %s:\t%d\n", argv[1], files_in_dir(argv[1], argv[2]));

	return 0;
}

size_t files_in_dir(const char* path, const char* permissions_str)
{
	//Convert text permissions to byte field
	unsigned int permissions = str_to_bit_field(permissions_str);

 	struct stat stats;
 	struct dirent* file_info;

 	size_t result = 0;

 	DIR* dir;
 	dir = opendir(path);
 	
 	int error_code = errno;
 	if(dir == NULL)
 	{
 		printf("Cannot open directory %s\n", path);
 		exit(error_code);
 	}

 	char tmp[256];

 	//count all files in directory
 	while((file_info = readdir(dir)) != NULL)
 	{
 		//dont count "." and ".."
 		if(strcmp(file_info->d_name, ".") != 0 && strcmp(file_info->d_name, "..") != 0)
 		{
 			strcpy(tmp, path);
 			strcat(tmp, "/");
 			strcat(tmp, file_info->d_name);
 			stat(tmp, &stats);
			
			//recursively count subdirs
 			if(S_ISDIR(stats.st_mode))
 			{
 				result += files_in_dir(tmp, permissions_str);
 			}
 			else
 			if(S_ISREG(stats.st_mode))
 			{
 				//511 DEC = 111 111 111 BIN - extract permissions part (youngest 9 bits)
 				//then compare with expected permissions
 				if((511 & stats.st_mode) == permissions) result++;
 			}
 		}
	}
 	closedir(dir);
 	return result;
}

unsigned int str_to_bit_field(const char* permissions)
{
	if(strlen(permissions)!=9){
		printf("Wrong permissions format.\n");
		exit(2);
	}

	unsigned int perms_bf = 0;

	if (permissions[0] == 'r') perms_bf |= S_IRUSR;   /* 3 bits for user  */
	if (permissions[1] == 'w') perms_bf |= S_IWUSR; 
	if (permissions[2] == 'x') perms_bf |= S_IXUSR;

	if (permissions[3] == 'r') perms_bf |= S_IRGRP;   /* 3 bits for group */
	if (permissions[4] == 'w') perms_bf |= S_IWGRP;
	if (permissions[5] == 'x') perms_bf |= S_IXGRP;

	if (permissions[6] == 'r') perms_bf |= S_IROTH;   /* 3 bits for other */
	if (permissions[7] == 'w') perms_bf |= S_IWOTH;
	if (permissions[8] == 'x') perms_bf |= S_IXOTH;

	return perms_bf;
}