#ifndef ZAD2B_H
#define ZAD2B_H

#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

time_t parseDate(char* date);
char* get_path(char* p);
int print(const char* filename, const struct stat* s, int flag);

#endif