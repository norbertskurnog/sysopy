#ifndef ZAD2A_H
#define ZAD2A_H

#include <stddef.h>

size_t files_in_dir(const char* path, const char* permissions);
unsigned int str_to_bit_field(const char* permissions);

#endif