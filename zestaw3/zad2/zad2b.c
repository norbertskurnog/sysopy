#include "zad2b.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <ftw.h>

#include <errno.h>

time_t data;
int op;

int main(int argc, char ** argv)
{
	struct stat s;

	if(argc<4)
	{
		printf("usage: %s dir <|>|= (dd-mm-rrrr)\n", argv[0]);
		exit(1);
	}
	stat(argv[1], &s);
	if(!S_ISDIR(s.st_mode))
	{
		printf("%s is not directory\n", argv[1]);
		exit(-1);
	}

	if(strcmp(argv[2], "<") == 0) op = -1;
	else if(strcmp(argv[2],"=") == 0) op = 0;
	else if(strcmp(argv[2],">") == 0) op = 1;
	else{
		printf("Unknown operation %s\n", argv[2]);
		exit(2);
	}

	data = parseDate(argv[3]);
	char* path = get_path(argv[1]);

	ftw(path, print, 1024);
	free(path);

	return 0;
}

time_t parseDate(char* date)
{
	struct tm t;
	t.tm_sec = 0;
	t.tm_min = 0;
	t.tm_hour = 0;
	t.tm_mday = atoi(strtok(date, "-"));
	t.tm_mon = atoi(strtok(NULL, "-"))-1;
	t.tm_year = atoi(strtok(NULL, "-"))-1900;

	return mktime(&t);
}

char* get_path(char* p)
{
	char* path = (char*)malloc(sizeof(char)*256);
	int error_code = errno;

	// zmienie working dir
	if(chdir(p) != 0)
	{
		error_code = errno;
		fprintf(stderr, "Path %s is incorrect\n", p);
		exit(error_code);
	}
	if(!getcwd(path, 256))
	{
		error_code = errno;
		fprintf(stderr, "Cannot get current working dir\n");
		exit(error_code);
	}
	return path;
}

//function to be called by ftw
int print(const char* filename, const struct stat *s, int flag)
{
	struct tm* t = localtime(&s->st_mtime);
	t->tm_sec = 0;
	t->tm_min = 0;
	t->tm_hour = 0;
	time_t file_date = mktime(t);

	if(flag == FTW_F){	
		switch(op){
			case -1:{
				if(file_date < data) printf("%s\n", filename);
				break;
			}
			case 0:{
				if(file_date == data) printf("%s\n", filename);
				break;
			}
			case 1:{
				if(file_date > data) printf("%s\n", filename);
				break;
			}
		}
	}

	return 0;
}