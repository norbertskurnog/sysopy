#ifndef ZAD1_H
#define ZAD1_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>

#include <signal.h>
#include <sys/types.h>

#include <math.h>
#include <unistd.h>

void* searcher_thread(void* args);
void cleanup(int param);

#endif