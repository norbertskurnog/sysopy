#include "zad1.h"

/* args */
char* str_to_find;
char* filename;
unsigned thread_num = 0;
size_t buffer_size = 0;

/* thread managment */
pthread_t* thread_arr;
int ret_val;
unsigned threads_running = 0;

/* file */
FILE* data_file;
sem_t* file_mutex;
pthread_mutex_t counter_mutex;

int i;
int main(int argc, char** argv)
{
	if(argc < 4){
		printf("Usage: %s thread_num filename buffer_size str\n", argv[0]);
		exit(1);
	}

	sscanf(argv[1], "%d", &thread_num);
	sscanf(argv[3], "%zd", &buffer_size);
	str_to_find = argv[4];
	filename = argv[2];

	signal(SIGINT, cleanup);

	/* open file to be searched in */
	data_file = fopen(filename, "r");
	if(!data_file){
		perror(filename);
		exit(errno);
	}

	/* open mutex for reading file */
	file_mutex = sem_open("file_mutex", O_CREAT, 0600, 1);

	/* allocate array for thread ids */
	thread_arr = (pthread_t*)malloc(thread_num*sizeof(pthread_t));

	#ifdef DETACHED
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

		fseek(data_file, 0, SEEK_END);
		int flen = ftell(data_file);
		fseek(data_file, 0, SEEK_SET);

		/* divide file by buffers */
		int buffs = ceil(flen/(double)buffer_size);

		/* numer of parts of file to be read by one thread */
		int fparts = buffs/thread_num;

		/* number of threads getting one more part to read */
		int more_parts = buffs % thread_num;
	#endif

	for(i=0; i<thread_num; i++){
		#ifndef DETACHED
			ret_val = pthread_create(&thread_arr[i], NULL, searcher_thread, NULL);
		#else
			int tmp_part_val = fparts+1;
			if(more_parts > 0){
				ret_val = pthread_create(&thread_arr[i], &attr, searcher_thread, (void*)&tmp_part_val);
				more_parts--;
			}
			else{
				ret_val = pthread_create(&thread_arr[i], &attr, searcher_thread, (void*)&fparts);
			}
		#endif

		threads_running++;

		if(ret_val != 0){
			perror("Thread create");
			exit(errno);
		}
	}

	while(threads_running > 0);

	raise(SIGINT);

	return 0;
}

void* searcher_thread(void* args)
{
	pthread_t tid = pthread_self();

	#ifdef DETACHED
		unsigned parts_to_read = *((unsigned*)args);
	#endif

	char* file_buffer = (char*)malloc(sizeof(char)*buffer_size);
	size_t bytes_read;
	char* found_str_pos;

	while(1){
		#ifdef SYNC
			int prev_state;
			pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &prev_state);
			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		#endif

		sem_wait(file_mutex);
		bytes_read = fread(file_buffer, sizeof(char), buffer_size, data_file);
		if(bytes_read == -1){
			perror("fread");
			break;
		}
		else{
			found_str_pos = strstr(file_buffer, str_to_find);
			if(found_str_pos > 0){
				int str_offset = (int)((found_str_pos - file_buffer)*sizeof(char));
				long pos_in_file = ftell(data_file) - bytes_read + str_offset;
				printf("\"%s\" found at %lu, thread id = %02x\n", str_to_find, pos_in_file, (unsigned)tid);
				printf("In string: \"%s\"\n", file_buffer);

				#if defined(ASYNC) || defined(SYNC)
					free(file_buffer);

					int j;
					for(j=0; j<thread_num; j++){
						pthread_t o_tid = thread_arr[j];
						if(o_tid != tid){
							pthread_cancel(o_tid);
							threads_running--;
						}
					}
					sleep(1);
					threads_running--;
					pthread_exit(NULL);
				#endif
			}
		}
		sem_post(file_mutex);

		#ifdef DETACHED
			parts_to_read--;
			if(!parts_to_read){
				threads_running--;
				pthread_exit(NULL);
			}
		#endif

		#ifdef SYNC
			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
			pthread_setcanceltype(prev_state, NULL);
			pthread_testcancel();
		#endif
	}

	return (void*)0;
}

void cleanup(int param)
{
	fclose(data_file);
	sem_unlink("file_mutex");
	free(thread_arr);
	exit(0);
}