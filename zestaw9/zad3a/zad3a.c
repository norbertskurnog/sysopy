#include "zad3a.h"

sem_t* forks[5];
pthread_t* phil;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int i;
int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	/* initialize fork semaphores */
	for(i=0; i<5; i++){
		forks[i] = (sem_t*)malloc(sizeof(sem_t));
		sem_init(forks[i], 0, 1);
	}

	/* initialize fork semaphores and philosopher threads*/
	phil = (pthread_t*)malloc(5*sizeof(pthread_t));
	for(i=0; i<5; i++){
		 /* prevent from reading changed i value */
		pthread_mutex_lock(&mutex);	
		pthread_create(&phil[i], NULL, phil_thread, (int*)&i);
	}
	pthread_mutex_destroy(&mutex);

	while(1);

	return 0;
}

void* phil_thread(void* args)
{
	int phil_no = *((int*)args);
	pthread_mutex_unlock(&mutex); /*variable read, unlock */

	int lfork = phil_no, rfork = (phil_no+1) % 5;

	/* take fork with lowest number first, release forks with highest numbers first */
	while(1){
		/* acquire forks */
		sem_wait(forks[lfork < rfork ? lfork : rfork]);
		sem_wait(forks[lfork < rfork ? rfork : lfork]);

		printf("Philosopher %d eating\n", phil_no);
		
		sleep(1);

		/* release forks */
		sem_post(forks[lfork > rfork ? lfork : rfork]);
		sem_post(forks[lfork > rfork ? rfork : lfork]);

		printf("Philosopher %d finished eating\n", phil_no);
		sleep(1);
	}

	return (void*)0;
}

void cleanup(int param)
{
	int j;
	for(j=0; j<5; j++){
		sem_destroy(forks[j]);
	}

	exit(0);
}