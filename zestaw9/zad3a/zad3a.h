#ifndef ZAD3A_H
#define ZAD3A_H

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <semaphore.h>
#include <fcntl.h>

#include <signal.h>

void* phil_thread(void* args);
void cleanup(int param);

#endif