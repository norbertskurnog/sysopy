#include "zad2.h"

/* args */
unsigned thread_num = 0;

/* thread managment */
pthread_t* thread_arr;
int ret_val;
unsigned threads_running = 0;

#ifdef P6
int divby0flag = 0;
#endif

int i;
int main(int argc, char** argv)
{
	if(argc < 2){
		printf("Usage: %s thread_num\n", argv[0]);
		exit(1);
	}

	// error checking?!
	sscanf(argv[1], "%d", &thread_num);

	signal(SIGINT, cleanup);

	#ifdef P2
	sigset_t tsigmask;
	sigemptyset(&tsigmask);

	sigaddset(&tsigmask, SIGTERM);
	sigaddset(&tsigmask, SIGUSR1);

	pthread_sigmask(SIG_SETMASK, &tsigmask, NULL);
	#endif

	#ifdef P3
		pthread_t tid =  pthread_self();
		printf("Main thread tid=%02x\n", (unsigned)tid);
	#endif

	/* allocate array for thread ids */
	thread_arr = (pthread_t*)malloc(thread_num*sizeof(pthread_t));

	for(i=0; i<thread_num; i++){

		ret_val = pthread_create(&thread_arr[i], NULL, searcher_thread, NULL);
		threads_running++;

		if(ret_val != 0){
			perror("Thread create");
			exit(errno);
		}
	}

	#if defined(P4) || defined(P5)
	sleep(3);
	int j;
	for(j=0; j<thread_num; j++){
		pthread_kill(thread_arr[j], SIGTERM);
	}
	sleep(1);
	for(j=0; j<thread_num; j++){
		pthread_kill(thread_arr[j], SIGUSR1);
	}
	#endif
	
	while(threads_running > 0);

	raise(SIGINT);

	return 0;
}

void* searcher_thread(void* args)
{
	#if !defined(P3) && !defined(P5) && !defined(P6)
	pthread_t tid = pthread_self();
	#endif

	#if defined(P3) || defined(P5)
		signal(SIGTERM, sigterm_thandler);
		signal(SIGUSR1, sigusr1_thandler);
	#endif

	#ifdef P4
	sigset_t tsigmask;
	sigemptyset(&tsigmask);

	sigaddset(&tsigmask, SIGTERM);
	sigaddset(&tsigmask, SIGUSR1);

	pthread_sigmask(SIG_SETMASK, &tsigmask, NULL);
	#endif

	#ifdef P6
	if(!divby0flag){
		int test = 15/0;
		printf("%d", test);
	}
	#endif

	while(1){
		#if !defined(P3) && !defined(P5) && !defined(P6)
		printf("Thread %02x running\n", (unsigned)tid);
		#endif
	}

	return (void*)0;
}

void cleanup(int param)
{
	free(thread_arr);
	exit(0);
}

#if defined(P3) || defined(P5)
void sigterm_thandler(int param)
{
	pthread_t tid = pthread_self();
	pid_t pid = getpid();
	printf("Got SIGTERM, pid=%d, tid=%02x\n", pid, (unsigned)tid);
}

void sigusr1_thandler(int param)
{
	pthread_t tid = pthread_self();
	pid_t pid = getpid();
	printf("Got SIGUSR1, pid=%d, tid=%02x\n", pid, (unsigned)tid);
}
#endif