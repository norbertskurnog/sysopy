#ifndef ZAD2_H
#define ZAD2_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

#include <math.h>

void* searcher_thread(void* args);
void cleanup(int param);

#if defined(P3) || defined(P5)
void sigusr1_thandler(int param);
void sigterm_thandler(int param);
#endif

#endif