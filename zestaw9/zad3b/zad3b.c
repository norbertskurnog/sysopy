#include "zad3b.h"

sem_t* forks[5];
pthread_t* phil;
pthread_cond_t* fork_perm;

pthread_mutex_t fork_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t condv_mutex = PTHREAD_MUTEX_INITIALIZER;

int i;
int main(int argc, char** argv)
{
	signal(SIGINT, cleanup);

	/* initalize philosopher conditional variables */
	fork_perm = (pthread_cond_t*)malloc(5*sizeof(pthread_cond_t));
	for(i=0; i<5; i++){
		pthread_cond_init(&fork_perm[i], NULL); 
	}

	/* initialize fork semaphores */
	for(i=0; i<5; i++){
		forks[i] = (sem_t*)malloc(sizeof(sem_t));
		sem_init(forks[i], 0, 1);
	}

	/* initialize philosopher threads*/
	int ids[5] = {0, 1, 2, 3, 4};

	phil = (pthread_t*)malloc(5*sizeof(pthread_t));
	for(i=0; i<5; i++){
		pthread_create(&phil[i], NULL, phil_thread, (int*)&ids[i]);
	}

	while(1);

	return 0;
}

void* phil_thread(void* args)
{
	printf("Thread %d running\n", *((int*)args));
	int phil_no = *((int*)args);

	int lfork = phil_no, rfork = (phil_no+1) % 5;
	int forkl_status, forkr_status; /* current philosopher has no forks */

	int phil_id;

	while(1){
		/* wait until both forks are free to take */
		pthread_mutex_lock(&condv_mutex);
		sem_getvalue(forks[lfork], &forkl_status);
		sem_getvalue(forks[rfork], &forkr_status);
		while(!(forkl_status && forkr_status)){
			pthread_cond_wait(&fork_perm[phil_no], &condv_mutex);
			sem_getvalue(forks[lfork], &forkl_status);
			sem_getvalue(forks[rfork], &forkr_status);
		}
		pthread_mutex_unlock(&condv_mutex);
		
		/* acquire forks */
		pthread_mutex_lock(&fork_mutex);
		sem_wait(forks[lfork]);
		sem_wait(forks[rfork]);
		pthread_mutex_unlock(&fork_mutex);

		printf("Philosopher %d eating\n", phil_no);
		
		sleep(1);
		/* release forks */
		pthread_mutex_lock(&fork_mutex);
		sem_post(forks[lfork]);
		phil_id = (phil_no-1)%5; /* % operator can return negarive value... */

		pthread_cond_signal(&fork_perm[phil_id<0 ? phil_id+5 : phil_id]);
		sem_post(forks[rfork]);
		pthread_cond_signal(&fork_perm[(phil_no+1)%5]);
		pthread_mutex_unlock(&fork_mutex);

		printf("Philosopher %d finished eating\n", phil_no);
		sleep(1);
	}

	return (void*)0;
}

void cleanup(int param)
{
	int j;
	for(j=0; j<5; j++){
		sem_destroy(forks[j]);
	}

	exit(0);
}