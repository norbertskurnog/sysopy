#ifndef ZAD1_H
#define ZAD1_H

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <string.h>

void handler (int parameter);
void sigint_handler(int parameter);
void print_backwards(char* string);

#endif