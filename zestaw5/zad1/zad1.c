#include "zad1.h"

char* line;
int cycle_length;

int iteration = 1;
int reversed = 0;

/* sigaction */

int main(int argc, char** argv)
{
	/* parsing cmd line */
	if(argc < 3){
		printf("Usage: %s text N\n", argv[0]);
		exit(1);
	}

	line = argv[1];
	if(strlen(line) < 1){
		printf("Invalid parameter %s\n", line);
		exit(2);
	}

	cycle_length = atoi(argv[2]);
	if(cycle_length < 1){
		printf("N must be positive integer\n");
		exit(1);
	}

	#ifndef SIGACTION
	signal(SIGTSTP, handler);
	signal(SIGINT, sigint_handler);
	#else
	struct sigaction new_action;
	memset(&new_action, 0, sizeof(new_action));
	new_action.sa_flags = 0;
	sigemptyset(&new_action.sa_mask);
	
	new_action.sa_handler = handler;
	sigaction(SIGTSTP, &new_action, NULL);

	new_action.sa_handler = sigint_handler;
	sigaction(SIGINT, &new_action, NULL);
	#endif

	while(1);

	return 0;
}

void handler (int parameter)
{
	int i;
	
	if(!reversed){
		for(i=0; i<iteration; i++){
			printf("%s\n", line);
		}
		reversed = 1;
	}
	else{
		for(i=0; i<iteration; i++){
			print_backwards(line);
		}
		reversed = 0;
		iteration++;
	}

	if(iteration > cycle_length) iteration=1;
}

void sigint_handler(int parameter)
{
	printf("Odebrano sygnał SIGINT\n");
	exit(0);
}

void print_backwards(char* str)
{
	int length = strlen(str);
	int i;
	for(i=length-1; i>=0; i--){
		printf("%c", str[i]);
	}

	printf("\n");
}