#include "sender.h"

int main(int argc, char const *argv[])
{
	/* command line */
	if(argc<3){
		printf("Usage: %s [kill|sigqueue param] pid\n", argv[0]);
		return 1;
	}

	/* use kill */
	if(!strcmp(argv[1], "kill") || !strcmp(argv[1], "-k")){
		pid_t pid = atoi(argv[2]);
		if(pid < 1){
			printf("Wrong pid %d\n", pid);
			return 2;
		}
		kill(pid, SIGUSR1);
	}

	/* use sigqueue */
	if(!strcmp(argv[1],"sigqueue") || !strcmp(argv[1], "-s")){		
		if(argc < 4){
			printf("Usage: %s [kill|sigqueue param] pid\n", argv[0]);
			return 1;
		}

		union sigval param = (union sigval)atoi(argv[2]);
		pid_t pid = atoi(argv[3]);

		if(pid < 1){
			printf("Wrong pid %d\n", pid);
			return 2;
		}

		/* send signal */
		int error = sigqueue(pid, SIGUSR1, param);
		if(error == -1){
			printf("Signal cannot be send\n");
			return 3;
		}
	}

	return 0;
}