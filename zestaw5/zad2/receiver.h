#ifndef RECEIVER_H
#define RECEIVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#include <unistd.h>
#include <sys/types.h>

#include <time.h>

void handler(int param, siginfo_t* siginfo, void* ucontext);

#endif