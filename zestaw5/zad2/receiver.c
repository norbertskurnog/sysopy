#include "receiver.h"

int main(int argc, char* argv[])
{
	struct sigaction new_action;
	memset(&new_action, 0, sizeof(new_action));

	new_action.sa_sigaction = handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = SA_SIGINFO;

	sigaction(SIGUSR1, &new_action, NULL);
	
	while(1);
	return 0;
}

void handler(int param, siginfo_t* siginfo, void* ucontext){
	printf("Signal info:\nReal UID: %d\nSender PID: %d\nUser time: %ju\n", siginfo->si_uid, siginfo->si_pid, siginfo->si_utime);
	printf("Value sent: %d\n", siginfo-> si_value);
	printf("\n");
	memset(siginfo, 0, sizeof(*siginfo));
}