#ifndef PARENT_H
#define PARENT_H

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <string.h>
#include <unistd.h>
#include <sys/types.h>

void handler_usr1(int param);
void handler_usr2(int param);

#endif