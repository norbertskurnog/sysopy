#ifndef CHILD_H
#define CHILD_H

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <unistd.h>
#include <sys/types.h>

void handler_usr1(int param);
void handler_usr2(int param);

#endif