#include "child.h"

unsigned counter = 0;
pid_t pid;

int main(int argc, char const *argv[])
{	
	signal(SIGRTMIN+1, handler_usr1);
	signal(SIGRTMIN, handler_usr2);

	pid = getppid();

	while(1);
	return 0;
}

void handler_usr1(int param)
{	
	counter++;
}

void handler_usr2(int param)
{
	int i=0;
	while(i<counter){
	 	sigqueue(pid, SIGRTMIN+1, (union sigval)0);
	 	i++;
	}
	//sleep(3);
	sigqueue(pid, SIGRTMIN, (union sigval)0);
	exit(0);
}