#include "child.h"

unsigned counter = 0;
pid_t pid;

int main(int argc, char const *argv[])
{	
	signal(SIGUSR1, handler_usr1);
	signal(SIGUSR2, handler_usr2);

	pid = getppid();

	while(1);
	return 0;
}

void handler_usr1(int param)
{	
	counter++;
}

void handler_usr2(int param)
{
	int i=0;
	while(i<counter){
	 	kill(pid, SIGUSR1);
	 	i++;
	}
	kill(pid, SIGUSR2);
	exit(0);
}