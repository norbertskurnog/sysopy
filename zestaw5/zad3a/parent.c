#include "parent.h"

int sig_counter = 0;
int signals;
pid_t pid;

int main(int argc, char const *argv[])
{
	if(argc < 2){
		printf("Usage: %s number_of_signals\n", argv[0]);
		return 1;
	}

	signals = atoi(argv[1]);
	if(signals < 1){
		printf("Number of signals must be positive integer\n");
		return 2;
	}

	signal(SIGUSR1, handler_usr1);
	signal(SIGUSR2, handler_usr2);

	pid = fork();

	if(pid == 0){
		/* child process */
		char* cmd[] = {"child", NULL};
		execv("./child", cmd);
		
		printf("Cannot exec program child\n");
		return 4;
	}
	else if(pid > 0){
		/* parent process */
		printf("Parent pid: %d\n", getpid());
		printf("Child pid: %d\n", pid);
		int i=0;
		sleep(1);
		while(i<signals){
			kill(pid, SIGUSR1);
			i++;
		}
		kill(pid, SIGUSR2);
		
		/* wait for transmission from child */
		while(1);
	}
	else{
		printf("Cannot fork!");
		return 3;
	}
	return 0;
}

void handler_usr1(int param)
{
	sig_counter++;
}

void handler_usr2(int param)
{
	printf("Received %d/%d signals\n", sig_counter, signals);
	exit(0);
}