#include "child.h"

unsigned counter = 0;
int transmission = 0;
int received = 0;
int ack = 1;
pid_t pid;

int main(int argc, char const *argv[])
{	
	signal(SIGUSR1, handler_usr1);
	signal(SIGUSR2, handler_usr2);

	pid = getppid();

	while(!transmission){
		if(received){
			counter++;
			received = 0;
			kill(pid, SIGUSR1);
		}
	}
	usleep(10);

	int i=0;
	while(i<counter){
	 	if(ack){
	  		ack = 0;
	  		i++;
	 		kill(pid, SIGUSR1);
	 	}
	}
	usleep(10);
	kill(pid, SIGUSR2);

	return 0;
}

void handler_usr1(int param)
{	
	if(!transmission){
		received = 1;
	}
	else{
	 	ack = 1;
	}
}

void handler_usr2(int param)
{
	transmission = !transmission;
}