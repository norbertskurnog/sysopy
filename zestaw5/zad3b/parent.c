#include "parent.h"

int sig_counter = 0;
int signals;
int ack = 1;
int transmission = 1;

int verbose = 0;
pid_t pid;

int main(int argc, char const *argv[])
{
	if(argc < 2){
		printf("Usage: %s [-v] number_of_signals\n", argv[0]);
		return 1;
	}

	signals = atoi(argv[argc-1]);
	if(signals < 1){
		printf("Number of signals must be positive integer\n");
		return 2;
	}
	if(argc > 2){
		if(!strcmp(argv[1], "-v")) verbose = 1;
	}

	signal(SIGUSR1, handler_usr1);
	signal(SIGUSR2, handler_usr2);

	pid = fork();

	if(pid == 0){
		/* child process */
		char* cmd[] = {"child", NULL};
		execv("./child", cmd);
		
		printf("Cannot exec program child\n");
		return 4;
	}
	else if(pid > 0){
		/* parent process */
		printf("Child pid: %d\n", pid);
		printf("Parent pid: %d\n", getpid());
		int i=0;
		
		sleep(1);
		while(i<signals){
			if(ack){
				ack = 0;
				kill(pid, SIGUSR1);
				i++;
				if(verbose) printf("Sent %d\n", i);
			}
		}
		kill(pid, SIGUSR2);
		
		/* wait for transmission from child */
		transmission = 0;

		while(1);
	}
	else{
		printf("Cannot fork!");
		return 3;
	}
	return 0;
}

void handler_usr1(int param)
{
	if(transmission) ack = 1;
	else{ /* receiving */
		sig_counter++;
		kill(pid, SIGUSR1); /* ACK */
		if(verbose) printf("Resent %d\n", sig_counter);
	} 
}

void handler_usr2(int param)
{
	printf("Received %d/%d signals\n", sig_counter, signals);
	exit(0);
}